const db = require('../database').db_rim; // as const knex = require('knex')(config);
const mailing = require('../plugins/mailing')
const pdfGen = require('../plugins/pdf-generator')
const moment = require('moment');


// MIDTRANS
exports.transPending = (inv_code) => {
    let mailerData = {
        template: 'payment-voucher-notyetpaid',
        subject: 'Complete your payment voucher Rimember',
        from: 'Rimember Orders'
    }
    db.select(
        't1.id',
        't1.fid_user',
        'point_cash_back',
        'cashback',
        'cashback_used',
        //
        't2.fullname',
        't2.email',
        't2.phone',
        't3.promo_title as promo_name',
        't3.promo_price',
        't1.quantity',
        't1.inv_code',
        't1.tax',
        't1.payment_type',
        't1.total',
        't1.redirect_url',
        't1.created as tgl_pembelian'
    )
        .from('ecommerce_transaction as t1')
        .innerJoin('members as t2', 't1.fid_user', 't2.id')
        .innerJoin('merchant_promo as t3', 't1.fid_promo', 't3.id')
        .where('t1.inv_code', inv_code)
        .then(data => {
            // console.log(moment(data[0].tgl_pembelian).format('dddd, DD MMMM YYYY'))
            if (data.length === 0){
                console.log('Pending: Data tidak di temukan')
                // res.json({
                //     success: false,
                //     message: "Data tidak di temukan",
                // });
            }else{
                mailing.sendEmailPending(data[0], mailerData.template, mailerData.subject, mailerData.from)
                if (data[0].cashback_used !== 0){
                    let q = db('members')
                        .where('id', data[0].fid_user)
                        .update({point_cash_back: 0})
                        .then(data2 => {
                            console.log('Pending: Update cashback success')
                            // res.json({
                            //     success: true,
                            //     message: "Update cashback success",
                            //     data: data,
                            //     // redirect_url: redirectUrl
                            // })

                        }).catch((err) =>{
                            console.log('Pending: '+err)
                            // res.json({
                            //     success: false,
                            //     message: "Update cashback failed.",
                            //     // count: data.length,
                            //     data: err,
                            // });
                        });
                }

            }
        });
}

exports.transSettle = (inv_code, settle_time) => {
    let mailerData = {
        template: 'payment-voucher',
        subject: 'Your Rimember Voucher Order Receipt',
        from: 'Rimember Receipt'
    }
    db.select(
        // t1
        't1.id',
        't1.fid_user',
        't1.fid_promo',
        't1.voucher_promo_code',
        't1.quantity',
        't1.inv_code',
        't1.voucher_code',
        't1.tax',
        't1.payment_type',
        't1.total',
        't1.redirect_url',
        't1.created as tgl_pembelian',
        't1.cashback',
        't1.cashback_used',
        't1.tz',
        // t2
        't2.point_cash_back',
        't2.fullname',
        't2.email',
        't2.phone',
        // t3
        't3.promo_title as promo_name',
        't3.promo_price',
        't3.soldout',
        // t4
        't4.id as mst_code_id',
        't4.code_price',
        't4.code_name',
        't4.code_for',
        't4.used',
    )
        .from('ecommerce_transaction as t1')
        .innerJoin('members as t2', 't1.fid_user', 't2.id')
        .innerJoin('merchant_promo as t3', 't1.fid_promo', 't3.id')
        .leftJoin('mst_code as t4', 't1.voucher_promo_code', 't4.code_name')
        .where('inv_code', inv_code)
        .then(data => {
            if (data.length === 0) {
                console.log('Settle : Data tidak di temukan')
            } else {
                // check code is for discount or cashback
                if (data[0].code_for === null || data[0].code_for === 'discount'){
                    data[0].code_price = 0
                }
                if(data[0].cashback_used !== 0){
                    db('members')
                        .where('id', data[0].fid_user)
                        .andWhere('email', data[0].email)
                        // #rules = set every he used the cashback it will be zero
                        .update({point_cash_back: 0})
                        .then((data6) => {
                            console.log('Settle : Add cashback to 0 success')
                            console.log(data6+' DATA')
                            db.select(
                                'point_cash_back'
                            )
                                .from('members')
                                .where('id', data[0].fid_user)
                                .andWhere('email', data[0].email)
                                .then((data7) => {
                                    console.log(data)
                                    // console.log( data7[0].point_cash_back)
                                    db('members')
                                        .where('id', data[0].fid_user)
                                        .update({point_cash_back: data7[0].point_cash_back + data[0].cashback + data[0].code_price})
                                        .then(data2 => {
                                            if (data[0].code_price !== 0){
                                                // ADD QUANTITY VOUCHER SOLD OUT
                                                db('mst_code')
                                                    .where('code_name', data[0].code_name)
                                                    .andWhere('id', data[0].mst_code_id)
                                                    .update({used: data[0].used + 1})
                                                    .then(data3 => {
                                                        console.log('Settle : Add Code Promo success and cashback success')
                                                    })
                                            }
                                            // console.log('Settle : Update cashback success')
                                        }).catch((err) => {
                                        console.log('Settle : ' + err)
                                    });
                                })
                        })
                }else{
                    db('members')
                        .where('id', data[0].fid_user)
                        .update({point_cash_back: data[0].point_cash_back + data[0].cashback + data[0].code_price})
                        .then(data2 => {
                            if (data[0].code_price !== 0){
                                // REDUCE QUANTITY VOUCHER
                                db('mst_code')
                                    .where('code_name', data[0].code_name)
                                    .andWhere('id', data[0].mst_code_id)
                                    .update({used: data[0].used + 1})
                                    .then(data3 => {
                                        console.log('Settle : Add Code Promo success and cashback success')
                                    })
                            }
                            // console.log('Settle : Update cashback success')
                        }).catch((err) => {
                        console.log('Settle : ' + err)
                    });
                }
                // ADD NOTIF
                let dataNotif = {
                    fid_user: data[0].fid_user,
                    mst_code_id: data[0].mst_code_id,
                    notif_type: 'cashback'
                }
                // ADD NOTIFICATION
                db('notifications')
                    .insert(dataNotif)
                    .then((data4) => {
                        console.log('Settle : Add notif success')
                    })
                // UPDATE PAID DATE
                console.log(settle_time, data[0].tz)
                let settlement_time = moment.utc(settle_time).utcOffset(-data[0].tz).format("YYYY-MM-DD HH:mm:ss")
                db('ecommerce_transaction')
                    .where('inv_code', inv_code)
                    .andWhere('id', data[0].id)
                    .update({paid_date: settlement_time})
                    .then((data8) => {
                        console.log('Settle : Update Paid Date success')
                    })
                // UPDATE QUANTITY VOUCHER
                // console.log(data[0].fid_promo, data[0].soldout)
                db('merchant_promo')
                    .where('id', data[0].fid_promo)
                    .update({soldout: data[0].soldout + 1})
                    .then((data5) => {
                        console.log('Settle : Add soldout success')
                    })
                data[0].code_price === null ? data[0].code_price = 0 : ''
                pdfGen.createVoucherReceiptPdf(data[0], res => {
                    console.log('payment: '+res)
                    // MAILING
                    mailing.sendEmailSettlement(data[0], mailerData.template, mailerData.subject, mailerData.from)
                })

            }
        })
        .catch((err) => {
            console.log('Settle : ' + err)
        });

}

exports.transFailure = (inv_code) => {
    // expire, pending, unverification
    // let page = req.params.page;
    // let id_user = req.userData.id
    // let remember_token = req.body.remember_token
    db.select(
        't1.id',
        'fid_user',
        'point_cash_back',
        'cashback',
        'cashback_used'
    )
        .from('ecommerce_transaction as t1')
        .innerJoin('members as t2', 't1.fid_user', 't2.id')
        .where('inv_code', inv_code)
        .then(data => {
            console.log(data)
            if (data.length === 0){
                console.log('Expire: Data tidak di temukan')
            }else{
                if (data[0].cashback_used !== 0){
                    let q = db('members')
                        .where('id', data[0].fid_user)
                        .update({point_cash_back: data[0].point_cash_back+data[0].cashback_used})
                        .then(data2 => {
                            console.log('Expire: Update cashback success')
                            // res.json({
                            //     success: true,
                            //     message: "Update cashback success",
                            //     data: data,
                            //     // redirect_url: redirectUrl
                            // })

                        }).catch((err) =>{
                            console.log(err)
                            // res.json({
                            //     success: false,
                            //     message: "Update cashback failed.",
                            //     // count: data.length,
                            //     data: err,
                            // });
                        });
                }

            }
        });

}

exports.transSettleDirect = (inv_code) => {
    // let page = req.params.page;
    // let id_user = req.userData.id
    // let remember_token = req.body.remember_token
    let _data = {}

    db.select(
        't1.id',
        'transaction_status',
        'fid_user',
        'point_cash_back',
        'cashback',
        'cashback_used'
    )
        .from('ecommerce_transaction as t1')
        .innerJoin('members as t2', 't1.fid_user', 't2.id')
        .where('inv_code', inv_code)
        .then(data => {
            if (data.length > 0){
                _data.transaction_status = 'failure'
                if (data[0].cashback_used !== 0){
                    if (data[0].transaction_status === 'unverification'){
                        _data.point_cash_back = data[0].point_cash_back+data[0].cashback_used
                    }

                    db('members')
                        .where('id', data[0].fid_user)
                        .update({point_cash_back: _data.point_cash_back})
                        .then(data2 => {
                            console.log('Direct: Update cashback success')

                        }).catch((err) =>{
                        console.log('Direct: '+err)
                    });
                }
                db('ecommerce_transaction')
                    .where('fid_user', data[0].fid_user)
                    .andWhere('inv_code', inv_code)
                    .andWhere('id', data[0].id)
                    .update({transaction_status: 'failure'})
                    .then(data2 => {
                        console.log('Direct: Update trans_status success')

                    }).catch((err) =>{
                    console.log('Direct: '+err)
                });

                console.log(data[0].transaction_status)
            }else{
                console.log('no data')
            }
        });
    // res.json({
    //     message: 'Post Created',
    //     authData
    // })
}

exports.transUnverification = (inv_code) => {
    // let page = req.params.page;
    // let id_user = req.userData.id
    // let remember_token = req.body.remember_token
    let _data = {}

    db.select(
        't1.id',
        'transaction_status',
        'fid_user',
        'point_cash_back',
        'cashback',
        'cashback_used'
    )
        .from('ecommerce_transaction as t1')
        .innerJoin('members as t2', 't1.fid_user', 't2.id')
        .where('inv_code', inv_code)
        .then(data => {
            if (data.length > 0){
                _data.transaction_status = 'failure'
                if (data[0].cashback_used !== 0){
                    if (data[0].transaction_status === 'unverification'){
                        _data.point_cash_back = data[0].point_cash_back+data[0].cashback_used
                    }

                    db('members')
                        .where('id', data[0].fid_user)
                        .update({point_cash_back: _data.point_cash_back})
                        .then(data2 => {
                            console.log('Direct: Update cashback success')

                        }).catch((err) =>{
                        console.log('Direct: '+err)
                    });
                }
                db('ecommerce_transaction')
                    .where('fid_user', data[0].fid_user)
                    .andWhere('inv_code', inv_code)
                    .andWhere('id', data[0].id)
                    .update({transaction_status: 'failure'})
                    .then(data2 => {
                        console.log('Direct: Update trans_status success')

                    }).catch((err) =>{
                    console.log('Direct: '+err)
                });

                console.log(data[0].transaction_status)
            }else{
                console.log('no data')
            }
        });
    // res.json({
    //     message: 'Post Created',
    //     authData
    // })
}

