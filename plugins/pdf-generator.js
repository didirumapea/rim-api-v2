const basepath =  __dirname.replace('plugins', '');
const currencyFormat = require('../plugins/currency-format')
const moment = require('moment');
const PDFDocument = require('pdfkit');
const qr = require('qr-image');
const fs = require('fs');
const cfg = require('../config/config');

exports.createVoucherReceiptPdf = (data, callback) => {
    // console.log(data)
    const dst = "/mnt/cdn/barcode-voucher/"+data.fid_user+data.voucher_code+".png"
    let qr_svg = qr.image(+data.fid_user+data.voucher_code, { type: 'png', margin: 2, ec_level: "H" });

    let _qr = qr_svg.pipe(require('fs').createWriteStream(dst))
    let tgl_beli = moment.utc(data.tgl_pembelian).utcOffset(data.tz).format("YYYY-MM-DD HH:mm:ss")
    _qr.on('close', () => {
// Create a document
        const doc = new PDFDocument;

// Pipe its output somewhere, like to a file or HTTP response
// See below for browser usage
        doc.pipe(fs.createWriteStream(cfg.assetPath + 'receipt-voucher-pdf/' + data.fid_user + data.inv_code + '.pdf'));

// line join settings
        doc.lineJoin('miter')
            .rect(50, 50, 500, 350)
            .stroke();
// draw some text
// doc.fontSize(25).text('Here is some vector graphics...', 100, 80);
        doc.fontSize(20).text('BUKTI PEMBELIAN (RECEIPT)', 60, 70);
        doc.fontSize(12).text('Nama', 60, 110);
        doc.fontSize(12).text('Email', 60, 150);
        doc.fontSize(12).text('Tanggal Pembelian', 60, 190);
        doc.fontSize(12).text('Item', 60, 230);
        doc.fontSize(12).text('Quantity', 60, 270);
        doc.fontSize(12).text('Kode Redeem', 60, 310);

        doc.fontSize(12).text(': ', 180, 110);
        doc.fontSize(12).text(': ', 180, 150);
        doc.fontSize(12).text(': ', 180, 190);
        doc.fontSize(12).text(': ', 180, 230);
        doc.fontSize(12).text(': ', 180, 230);
        doc.fontSize(12).text(': ', 180, 270);
        doc.fontSize(12).text(': ', 180, 310);

        doc.fontSize(12).text(data.fullname, 190, 110);
        doc.fontSize(12).text(data.email, 190, 150);
        doc.fontSize(12).text(tgl_beli, 190, 190);
        doc.fontSize(12).text(data.promo_name, 190, 230);
        doc.fontSize(12).text(data.quantity, 190, 270);
        doc.fontSize(12).text(data.voucher_code, 190, 310);

        doc.fontSize(12).text('Pastikan anda menukarkan kode redeem di merchant pilihan anda dan voucher dapat di gunakan 1x24 jam setelah pembelian.', 60, 360, {
            align: 'center'
        });
// Embed a font, set the font size, and render some text
// doc.font(basepath+'public/font/PalatinoBold.ttf')
//    .fontSize(25)
//    .text('Some text with an embedded font!', 100, 100);

// Add an image, constrain it to a given size, and center it vertically and horizontally
//     doc.image(basepath + 'public/images/receipt-paid.png', 175, 450, {
//       fit: [250, 250],
//       align: 'center',
//       valign: 'center'
//     });

        doc.image(basepath+'public/images/receipt-paid.png', 80, 450, {
            fit: [250, 250],
            align: 'center',
            valign: 'center'
        });

        doc.image('/mnt/cdn/barcode-voucher/'+data.fid_user+data.voucher_code+'.png', 350, 500, {
            fit: [150, 150],
            align: 'center',
            valign: 'center'
        });

// Add another page
// doc.addPage()
//    .fontSize(25)
//    .text('Here is some vector graphics...', 100, 100);

// Draw a triangle
// doc.save()
//    .moveTo(100, 150)
//    .lineTo(100, 250)
//    .lineTo(200, 250)
//    .fill("#FF3300");

// Apply some transforms and render an SVG path with the 'even-odd' fill rule
// doc.scale(0.6)
//    .translate(470, -380)
//    .path('M 250,75 L 323,301 131,161 369,161 177,301 z')
//    .fill('red', 'even-odd')
//    .restore();

// Add some text with annotations
// doc.addPage()
//    .fillColor("blue")
//    .text('Here is a link!', 100, 100)
//    .underline(100, 100, 160, 27, {color: "#0000FF"})
//    .link(100, 100, 160, 27, 'http://google.com/');

// Finalize PDF file
        doc.end();
    })
    // console.log('create voucher receipt succeed!')
    return callback('create voucher receipt succeed!')
// console.log('create voucher receipt succeed!')
//   res.json({
//     success: true,
//     message: "Success Create PDF",
//     data: 'data'
//   })
};


