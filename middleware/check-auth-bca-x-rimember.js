const jwt = require('jsonwebtoken')
const config = require('../config/config')

module.exports = (req, res, next) => {
    try{
        // console.log(process.env.JWT_KEY)
        const token = req.headers.authorization.split(' ')[1]
        req.userData = jwt.verify(token, config.jwtSecretKeyBcaResponse);
        next();
    }catch (error) {
        return res.status(401).json({
            message: 'auth Failed'
        })
    }
}