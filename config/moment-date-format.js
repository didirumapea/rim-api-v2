//
const moment = require('moment')

function MOMENT_CONSTRUCTOR() {
    this.utc7 = () => {
        return moment.utc().utcOffset('+07:00').format("YYYY-MM-DD HH:mm:ss")
    }
    this.localDate = () => {
        return moment().format("YYYY-MM-DD")
    }

    this.localDateWithTime = () => {
        return moment().format("YYYY-MM-DD HH:mm:ss")
    }

    this.dateLast7days = () => {
        return moment.utc().subtract(7, 'days').utcOffset('+07:00').format("YYYY-MM-DD HH:mm:ss")

    }

    this.utc = () => {
        return moment.utc().format("YYYY-MM-DD HH:mm:ss")

    }

    this.getTimezoneOffset = () => {
        return moment().utcOffset()/60

    }

    this.getUtcTimezoneWithOffset = (offset, date) => {
        // console.log(date+' dates')
        if (date === 0){
            return moment.utc().utcOffset(offset).format("YYYY-MM-DD HH:mm:ss")
        }else{
            return moment.utc(date).utcOffset(offset).format("YYYY-MM-DD HH:mm:ss")
        }


    }

    this.getUtcTimeWithAddDays = (days) => {
        return moment.utc().add(days, 'days').format("YYYY-MM-DD hh:mm:ss")

    }


    // console.log(moment.utc().format("YYYY-MM-DD hh:mm:ss"))
    // console.log(moment().utcOffset()/60) // GET TIMEZONE OFFSET ex: -7, -6 .... -2, -1, +1, +2, +3 .... +7
    // console.log(moment.utc().utcOffset(7).format("YYYY-MM-DD HH:mm:ss"))
    this.utcOffset = '+07:00'
}

module.exports = new MOMENT_CONSTRUCTOR()

// module.exports = {
//     utcOffset: '+07:00',
//     utcTime: new utcTime7()
// };