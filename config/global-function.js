//
require('babel-polyfill');
const brandedQRCode = require('branded-qr-code');
const qr = require('qr-image');
const fs = require('fs');
const path = require('path');
// const dirPath = path.join(__dirname, '/swamp-pix');

function GENERATE_BARCODE_FUNCTION_CONSTRUCTOR() {
    // BARCODE WITH LOGO
    this.generateBarcode = (text) => {
        // const dst = path.resolve(__dirname, `../../public/images/output/KG-WS0390190232.png`);
        let destinationBarcode = 'barcode-member/'
        if (text.startsWith('KG-')){
            destinationBarcode = 'barcode-merchant/'
        }

        let qr_svg = qr.image(text, { type: 'png', margin: 2, ec_level: "H" });
        qr_svg.pipe(require('fs').createWriteStream('/mnt/cdn/barcode/'+destinationBarcode+text+'.png'));

        // let svg_string = qr.imageSync('I love QR!', { type: 'png' });

        return true
    }
    // BARCODE STANDAR
    this.generateBarcodeWithLogo = (text) => {
        let destinationBarcode = 'barcode-member/'
        let logoBarcode = '/mnt/cdn/barcode/logo/logo/Logo-Rimember-2-05-resized.png'
        if (text.startsWith('KG-')){
            console.log('its merchant')
            destinationBarcode = 'barcode-merchant/'
        }
        // Or old good syntax
        // require('babel-polyfill');
        // var brandedQRCode = require('branded-qr-code');

        // Return a buffer with the PNG of the code
        // brandedQRCode.generate({text: 'https://www.google.com', path: '/Users/didi-rim-01/Downloads/logo.jpeg'});
        const logoPath = path.resolve(__dirname, `/mnt/cdn/barcode/logo/logo/Logo-Rimember-2-05-resized.png`);
        // const dst = path.resolve(__dirname, `../../public/images/output/KG-WS0390190232.png`);
        const dst = path.resolve(`/mnt/cdn/barcode/`+destinationBarcode+text+'.png');
        brandedQRCode.generate({
            text: text,
            path: logoPath,
            ratio: 1.8,
            opts: {
                errorCorrectionLevel: "H" || 'Q',
                margin: 2,
                getLogoPath: false
            },
        }).then((buf) => {
            fs.writeFile(dst, buf, (err) => {
                if (err) {
                    return console.log(err);
                }
                return console.log(dst, 'saved.');
            });
        });
// For express if you need different logos:
// app.get('/qr/:logo', route({ getLogoPath: req => `../images/original/${req.params.logo}.png` }));

        // res.json({
        //     success: true,
        //     msg: "generate barcode berhasil",
        //     data: "data"
        // })

        return true
    }

}

function SPLIT_IMAGE_FUNCTION_CONSTRUCTOR() {
    // SPLIT IMAGE TNC & BENEFIT
    this.splitTnc = (benefit, tnc) => {
        let objBenefit = {}
        let objTnc = {}
        let objd2 = []
        let cnt = -1
        let cnt2 = -1
        // console.log(benefit)
        // PARSING BENEFIT
        benefit.split('\n').forEach((element, index) => {
            if (element !== ''){
                // console.log(element)
                if (element.startsWith('###')){
                    // val1.push(element)
                    objd2[cnt2].subtitle.push(element.replace(/#/g, ''))
                }
                else if(element.startsWith('##')){

                    cnt2++;
                    objd2[cnt2] = {
                        title: element.replace(/#/g, ''),
                        subtitle: []
                    }
                    objBenefit[cnt].subtitle.push(objd2[cnt2])
                }else{
                    // console.log(objd2)
                    cnt++;
                    objBenefit[cnt] = {
                        title: element.replace(/#/g, ''),
                        subtitle: []
                    }
                }
            }
        })

        objd2 = []
        cnt = -1
        cnt2 = -1
        //PARSING TNC
        tnc.split('\n').forEach((element, index) => {
            if (element !== ''){
                // console.log(element)
                if (element.startsWith('###')){
                    // val1.push(element)
                    objd2[cnt2].subtitle.push(element.replace(/#/g, ''))
                }
                else if(element.startsWith('##')){

                    cnt2++;
                    objd2[cnt2] = {
                        title: element.replace(/#/g, ''),
                        subtitle: []
                    }
                    objTnc[cnt].subtitle.push(objd2[cnt2])
                }else if(element.startsWith('#')){
                    // console.log(objd2)
                    cnt++;
                    objTnc[cnt] = {
                        title: element.replace(/#/g, ''),
                        subtitle: []
                    }
                }else{

                }
            }
        })
        // console.log(val1, val2)
        // console.log(objBenefit)
        return {objBenefit, objTnc}
    }
}

module.exports = {
    genBarcode: new GENERATE_BARCODE_FUNCTION_CONSTRUCTOR,
    splitImage: new SPLIT_IMAGE_FUNCTION_CONSTRUCTOR
}