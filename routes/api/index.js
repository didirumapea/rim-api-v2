const express = require('express');
const router = express.Router();
const checkAuth = require('../../middleware/check-auth')
const auth = require('./auth/auth');
const auth_merchant = require('./auth-merchant/auth-merchant');
const user = require('./user/user');
const home = require('./home/home');
const category = require('./category/category');
const product = require('./product/product');
const dashboard = require('./dashboard/dashboard');
const payment = require('./payment/payment');
const merchant = require('./merchant/merchant');
const masterdata = require('./masterdata/masterdata');
const code = require('./code/code');
// const cms_transaction = require('./cms-transaction/cms-transaction');
const bank = require('./bank/bank');
const bca = require('./bca-endpoint');

router.use('/auth', auth);
router.use('/auth-merchant', auth_merchant);
router.use('/home', home);
router.use('/user', checkAuth, user);
router.use('/category', category);
router.use('/product', product);
router.use('/dashboard', dashboard);
router.use('/payment', payment);
router.use('/merchant', merchant);
router.use('/masterdata', masterdata);
router.use('/code', code);
router.use('/bank', bank);
router.use('/va', bca);
// router.use('/cms-transaction', cms_transaction);

module.exports = router;