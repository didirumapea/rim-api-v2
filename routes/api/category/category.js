const express = require('express');
const router = express.Router();
const db = require('../../../database').db_rim; // as const knex = require('knex')(config);
const moment = require('moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const date = require('../../../config/moment-date-format')


let dateNow = moment().format("YYYY-MM-DD hh:mm:ss")

router.get('/', (req, res) => {
    // console.log(req.query)
    res.send('We Are In Category Route')
})

// FILTER
// router.get('/catId=:catId/page=:page/limit=:limit/sort=:sort/min=:min/max=:max/rating=:rating',  (req, res) => {
router.get('/catId=:catId/page=:page/limit=:limit/sort=:sort/min=:min/max=:max/rating=:rating',  (req, res) => {
    let catId = req.params.catId
    let page = req.params.page
    let limit = req.params.limit
    let sort = req.params.sort
    let min = req.params.min
    let max = req.params.max
    let rating = req.params.rating
    // console.log(catId, page, limit, sort, min, max, rating)
    // console.log(req)
    db.select(
        't1.id',
        't1.fid_merchant',
        't2.default_image as merchant_image',
        't3.title as category',
        't3.slug as category_slug',
        't2.title as merchant_title',
        't2.address as merchant_address',
        't2.location as merchant_location',
        't2.city as merchant_city',
        'promo_title',
        't1.slug',
        'promo_desc',
        'benefit',
        'basic_price',
        'promo_price',
        't1.cashback_percentage',
        't1.max_cashback',
        't1.start_date',
        't1.end_date',
        'tnc',
        'expired',
        'images',
        'quantity',
        'soldout',
        'hotdeals',
    )
        .from('merchant_promo as t1')
        .innerJoin('merchant as t2', 't2.id', 't1.fid_merchant')
        .innerJoin('merchant_category as t3','t2.fid_category', 't3.id')
        .andWhere('t1.activated', '=', 1)
        .andWhere('quantity', '>', 0)
        .andWhere('t2.fid_category', catId)
        // .andWhere('t2.city', 'Bandung')
        .andWhereBetween('t1.promo_price', [min, max])
        .andWhere('end_date', '>', date.localDate())
        .orderBy('t1.id', sort)
        .paginate(limit, page, true)
        .then(paginator => {
            // console.log(paginator)
            let bigImage = []
            let thumbImage = []
            paginator.data.forEach((element1, index1) => {
                element1.images.split(',').forEach((element2, index2) => {
                    if (element2.startsWith('big')){
                        // thumbImage['img']
                        bigImage.push(element2)
                    }else{
                        thumbImage.push(element2)
                    }
                    // console.log(index+'. '+element)
                    // img['img'+index] = element
                })
                paginator.data[index1].images = {thumbImage: thumbImage, bigImage: bigImage}
                thumbImage = []
                bigImage = []
            })

            // console.log(paginator)
            if (paginator.total !== 0){
                res.json({
                    success: true,
                    message: 'Sukses ambil data '+paginator.data[0].category,
                    count: paginator.total,
                    current_page: paginator.current_page,
                    last_page: paginator.last_page,
                    limit: paginator.per_page,
                    // sortBy: sortBy,
                    data: paginator.data,
                });
            }else{
                res.json({
                    // name: data
                    success: true,
                    message: "Data Kosong",
                    count: paginator.total,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }
        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'something error check your server logs.',
                data: err.toLocaleString()
            });
            console.log(err)
        });

});

router.get('/page=:page/limit=:limit/sort=:sort/min=:min/max=:max/sort-by=:sortby/rating=:rating',  (req, res) => {
    let catId = req.params.catId
    let page = req.params.page
    let limit = req.params.limit
    let sort = req.params.sort
    let min = req.params.min
    let max = req.params.max
    let rating = req.params.rating
    let sortby = req.params.sortby
    // HargaTermurah(limit, page, sort, req, res)
    // console.log(catId, page, limit, sort, min, max, rating)
    // console.log(date.localDate())

    let q =  db.select(
        't1.id',
        't1.fid_merchant',
        't4.title as category',
        't2.title',
        't2.address',
        't2.location',
        't2.city',
        'promo_title',
        't1.slug',
        'promo_desc',
        'benefit',
        'basic_price',
        'promo_price',
        't1.cashback_percentage',
        't1.start_date',
        't1.end_date',
        'tnc',
        'expired',
        'images',
        'quantity',
        'soldout',
        'hotdeals',
        't1.created',
    )
        .from('merchant_promo as t1')
        .innerJoin('merchant as t2', 't2.id', 't1.fid_merchant')
        .innerJoin('merchant_category as t4','t2.fid_category', 't4.id')
        .andWhere('t1.activated', '=', 1)
        .andWhere('quantity', '>', 0)
        .andWhere('end_date', '>', date.localDate())
        .andWhereBetween('t1.promo_price', [min, max]);
        // .andWhere('t2.fid_category', catId)
        // .andWhere('t2.city', 'Bandung')
        // .andWhereBetween('t1.promo_price', [min, max])
        // .orderBy('t1.promo_price', sort);
        switch(sortby){
            case 'promo-terbaru':
                q.andWhere('t1.created', '<=', date.localDateWithTime())
                .orderBy('t1.created', 'desc');
                break;
            case 'rating-tertinggi':
                break;
            case 'harga-termurah':
                q.orderBy('t1.promo_price', 'asc');
                break;
            case 'paling-populer':
                q.orderBy('t1.soldout', 'desc');
                break;
            default:

                break;
        }

        q.paginate(limit, page, true)
        .then(paginator => {
            // console.log(paginator)
            let bigImage = []
            let thumbImage = []
            paginator.data.forEach((element1, index1) => {
                element1.images.split(',').forEach((element2, index2) => {
                    if (element2.startsWith('big')){
                        // thumbImage['img']
                        bigImage.push(element2)
                    }else{
                        thumbImage.push(element2)
                    }
                    // console.log(index+'. '+element)
                    // img['img'+index] = element
                })
                paginator.data[index1].images = {thumbImage: thumbImage, bigImage: bigImage}
                thumbImage = []
                bigImage = []
            })

            // console.log(paginator)
            if (paginator.total !== 0){
                res.json({
                    success: true,
                    message: 'Sukses ambil data '+paginator.data[0].category,
                    count: paginator.total,
                    current_page: paginator.current_page,
                    last_page: paginator.last_page,
                    limit: paginator.per_page,
                    // sortBy: sortBy,
                    data: paginator.data,
                });
            }else{
                res.json({
                    // name: data
                    success: true,
                    message: "Data Kosong",
                    count: paginator.total,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }
        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'something error check your server logs.',
                data: err.toLocaleString()
            });
            console.log(err)
        });


});

//FILTER WEB
router.get('/catId=:catId/page=:page/limit=:limit/sort=:sort/min=:min/max=:max/sort-by=:sortby/rating=:rating',  (req, res) => {
    let catId = req.params.catId
    let page = req.params.page
    let limit = req.params.limit
    let sort = req.params.sort
    let min = req.params.min
    let max = req.params.max
    let rating = req.params.rating
    let sortby = req.params.sortby
    // HargaTermurah(limit, page, sort, req, res)
    // console.log(catId, page, limit, sort, min, max, rating)
    // console.log(date.localDate())

    let q =  db.select(
        't1.id',
        't1.fid_merchant',
        't2.default_image as merchant_image',
        't3.title as category',
        't3.slug as category_slug',
        't2.title as merchant_title',
        't2.address as merchant_address',
        't2.location as merchant_location',
        't2.city as merchant_city',
        'promo_title',
        't1.slug',
        'promo_desc',
        'benefit',
        'basic_price',
        'promo_price',
        't1.cashback_percentage',
        't1.max_cashback',
        't1.start_date',
        't1.end_date',
        'tnc',
        'expired',
        'images',
        'quantity',
        'soldout',
        'hotdeals',
    )
        .from('merchant_promo as t1')
        .innerJoin('merchant as t2', 't2.id', 't1.fid_merchant')
        .innerJoin('merchant_category as t3','t2.fid_category', 't3.id')
        .andWhere('t1.activated', '=', 1)
        .andWhere('quantity', '>', 0)
        .andWhere('end_date', '>', date.localDate())
        .andWhere('t2.fid_category', catId)
        .andWhereBetween('t1.promo_price', [min, max]);
    // .andWhere('t2.city', 'Bandung')
    // .andWhereBetween('t1.promo_price', [min, max])
    // .orderBy('t1.promo_price', sort);
    switch(sortby){
        case 'promo-terbaru':
            q.andWhere('t1.created', '<=', date.localDateWithTime())
                .orderBy('t1.id', 'desc');
            break;
        case 'rating-tertinggi':
            break;
        case 'harga-termurah':
            q.orderBy('t1.promo_price', 'asc');
            break;
        case 'paling-populer':
            q.orderBy('t1.soldout', 'desc');
            break;
        default:

            break;
    }

    q.paginate(limit, page, true)
        .then(paginator => {
            // console.log(paginator)
            let bigImage = []
            let thumbImage = []
            paginator.data.forEach((element1, index1) => {
                element1.images.split(',').forEach((element2, index2) => {
                    if (element2.startsWith('big')){
                        // thumbImage['img']
                        bigImage.push(element2)
                    }else{
                        thumbImage.push(element2)
                    }
                    // console.log(index+'. '+element)
                    // img['img'+index] = element
                })
                paginator.data[index1].images = {thumbImage: thumbImage, bigImage: bigImage}
                thumbImage = []
                bigImage = []
            })

            // console.log(paginator)
            if (paginator.total !== 0){
                res.json({
                    success: true,
                    message: 'Sukses ambil data '+paginator.data[0].category,
                    count: paginator.total,
                    current_page: paginator.current_page,
                    last_page: paginator.last_page,
                    limit: paginator.per_page,
                    // sortBy: sortBy,
                    data: paginator.data,
                });
            }else{
                res.json({
                    // name: data
                    success: true,
                    message: "Data Kosong",
                    count: paginator.total,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }
        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'something error check your server logs.',
                data: err.toLocaleString()
            });
            console.log(err)
        });


});



module.exports = router;