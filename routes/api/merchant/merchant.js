const express = require('express');
const router = express.Router();
const db = require('../../../database').db_rim; // as const knex = require('knex')(config);
const moment = require('moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const date = require('../../../config/moment-date-format')
const globalFunc = require('../../../config/global-function')



let dateNow = moment().format("YYYY-MM-DD hh:mm:ss")

router.get('/', (req, res) => {
    // console.log(req.query)
    res.send('We Are In MERCHANT Route')
})

// GET LIST
router.get('/list/page=:page/limit=:limit/sort=:sort', (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;

    db.select('*')
        .from('merchant')
        .where('activated', 1)
        .orderBy('id', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            // paginator.data[0].coordinat2 = JSON.parse(paginator.data[0].coordinat2)
            // console.log(paginator)
            res.json({
                success: true,
                message: "sukses ambil data merchant",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: sortBy,
                data: paginator.data,
            });
        });


});

// GET LIST BY MERCHANT ID
router.get('/id=:id/page=:page/limit=:limit/sort=:sort', (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    let id = req.params.id

    db.select('*')
        .from('merchant')
        .where('activated', 1)
        .andWhere('id', id)
        .orderBy('id', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            // paginator.data[0].coordinat2 = JSON.parse(paginator.data[0].coordinat2)
            // console.log(paginator)
            res.json({
                success: true,
                message: "sukses ambil data merchant",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: sortBy,
                data: paginator.data,
            });
        });


});

// GET LIST PRODUCT BY MERCHANT ID
router.get('/product/merchant_id=:merchant_id/page=:page/limit=:limit/sort=:sort',  (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    let merchant_id = req.params.merchant_id
    db.select(
        't1.id',
        't1.fid_merchant',
        't2.default_image as merchant_image',
        't3.title as category',
        't2.title as merchant_title',
        't2.address as merchant_address',
        't2.location as merchant_location',
        't2.city as merchant_city',
        'promo_title',
        't1.slug',
        'promo_desc',
        'benefit',
        'basic_price',
        'promo_price',
        't1.cashback_percentage',
        't1.max_cashback',
        't1.start_date',
        't1.end_date',
        'tnc',
        'expired',
        'images',
        'quantity',
        'soldout',
        'hotdeals',
    )
        .from('merchant_promo as t1')
        .innerJoin('merchant as t2', 't2.id', 't1.fid_merchant')
        .innerJoin('merchant_category as t3','t2.fid_category', 't3.id')
        .andWhere('t1.activated',1)
        .andWhere('t2.activated',1)
        .andWhere('quantity', '>', 0)
        // .andWhere('t1.fid_account_type', 1)
        // .andWhere('t2.city', 'Bandung')
        // .andWhere('t1.fid_merchant', merchant_id)
        .andWhere('end_date', '>', date.localDate())
        // .orderBy('t1.id', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length > 0){
                // console.log(data)
                let bigImage = []
                let thumbImage = []
                // let tncSpl = splitImage.splitTnc(paginator.data[0].benefit, paginator.data[0].tnc)
                // paginator.data[0].benefit = tncSpl.objBenefit
                // paginator.data[0].tnc = tncSpl.objTnc
                // console.log(tncSpl)
                paginator.data.forEach((element1, index1) => {
                    element1.images.split(',').forEach((element2, index2) => {
                        if (element2.startsWith('big')){
                            // thumbImage['img']
                            bigImage.push(element2)
                        }else{
                            thumbImage.push(element2)
                        }
                        // console.log(index+'. '+element)
                        // img['img'+index] = element
                    })
                    paginator.data[index1].images = {thumbImage: thumbImage, bigImage: bigImage}
                    thumbImage = []
                    bigImage = []
                })
                res.json({
                    success: true,
                    message: 'Sukses ambil data list product dari merchant '+paginator.data[0].merchant_title,
                    count: paginator.total,
                    current_page: paginator.current_page,
                    last_page: paginator.last_page,
                    limit: paginator.per_page,
                    // sortBy: sortBy,
                    data: paginator.data,
                    // data2: tncSpl,
                });
            }else{
                res.json({
                    // name: data
                    success: true,
                    message: "Data Kosong",
                    // count: paginate.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }
        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'something error check your server logs.',
                data: err.toLocaleString()
            });
            console.log(err)
        });

});

// GET LIST PRODUCT BY BARCODE
router.get('/product/barcode=:barcode/page=:page/limit=:limit/sort=:sort',  (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    let barcode = req.params.barcode
    db.select(
        't1.id',
        't1.fid_merchant',
        't2.default_image as merchant_image',
        't3.title as category',
        't2.title as merchant_title',
        't2.address as merchant_address',
        't2.location as merchant_location',
        't2.city as merchant_city',
        'promo_title',
        't1.slug',
        'promo_desc',
        'benefit',
        'basic_price',
        'promo_price',
        't1.cashback_percentage',
        't1.max_cashback',
        't1.start_date',
        't1.end_date',
        'tnc',
        'expired',
        'images',
        'quantity',
        'soldout',
        'hotdeals',
    )
        .from('merchant_promo as t1')
        .innerJoin('merchant as t2', 't2.id', 't1.fid_merchant')
        .innerJoin('merchant_category as t3','t2.fid_category', 't3.id')
        .andWhere('t1.activated',1)
        .andWhere('quantity', '>', 0)
        .andWhere('t1.fid_account_type', 1)
        // .andWhere('t2.city', 'Bandung')
        .andWhere('t2.barcode', barcode)
        .andWhere('end_date', '>', date.localDate())
        // .orderBy('t1.id', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length > 0){
                // console.log(data)
                let bigImage = []
                let thumbImage = []
                // let tncSpl = splitImage.splitTnc(paginator.data[0].benefit, paginator.data[0].tnc)
                // paginator.data[0].benefit = tncSpl.objBenefit
                // paginator.data[0].tnc = tncSpl.objTnc
                // console.log(tncSpl)
                paginator.data.forEach((element1, index1) => {
                    element1.images.split(',').forEach((element2, index2) => {
                        if (element2.startsWith('big')){
                            // thumbImage['img']
                            bigImage.push(element2)
                        }else{
                            thumbImage.push(element2)
                        }
                        // console.log(index+'. '+element)
                        // img['img'+index] = element
                    })
                    paginator.data[index1].images = {thumbImage: thumbImage, bigImage: bigImage}
                    thumbImage = []
                    bigImage = []
                })
                res.json({
                    success: true,
                    message: 'Sukses ambil data list product dari merchant '+paginator.data[0].merchant_title,
                    count: paginator.total,
                    current_page: paginator.current_page,
                    last_page: paginator.last_page,
                    limit: paginator.per_page,
                    // sortBy: sortBy,
                    data: paginator.data,
                    // data2: tncSpl,
                });
            }else{
                res.json({
                    // name: data
                    success: true,
                    message: "Data Kosong",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }
        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'something error check your server logs.',
                data: err.toLocaleString()
            });
            console.log(err)
        });

});

// SEARCH
router.get('/search/value=:value/page=:page/limit=:limit/sort=:sort', (req, res) => {
    let value = req.params.value;
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;

    db.select('*')
        .from('merchant')
        .where('activated', 1)
        .whereRaw(`LOWER(title) LIKE LOWER(?)`, [`%${value}%`])
        .orderBy('id', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            // paginator.data[0].coordinat2 = JSON.parse(paginator.data[0].coordinat2)
            // console.log(paginator)
            res.json({
                success: true,
                message: "sukses ambil data merchant",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: sortBy,
                data: paginator.data,
            });
        });


});

// NEARBY
router.post('/nearby/page=:page/limit=:limit/sort=:sort', (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    let city = req.body.city
    let location = req.body.location
    let cat_id = req.body.cat_id
    // console.log(req.body)
    let q = db.select(
        '*',
    )
        .from('merchant as t1')
        // .innerJoin('merchant_category as t2', 't1.fid_category', 't2.id')
        .where('t1.activated', 1);
        if (cat_id !== "0" && city !== "0" && location !== "0"){
            q.whereRaw('(LOWER(city) = LOWER(?) AND fid_category = ?) OR (LOWER(location) = LOWER(?) AND fid_category = ?)', [city, cat_id, location, cat_id])
        }else{
            q.whereRaw('LOWER(city) = LOWER(?) OR LOWER(location) = LOWER(?)', [city, location])
        }
        q.orderBy('id', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            // paginator.data[0].coordinat2 = JSON.parse(paginator.data[0].coordinat2)
            // console.log(paginator)
            res.json({
                success: true,
                message: "sukses ambil data nearby merchant",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: sortBy,
                data: paginator.data,
            });
        })
            .catch((err) => {
                res.json({
                    // name: data
                    success: false,
                    message: 'something error check your server logs.',
                    data: err.toLocaleString()
                });
                console.log(err)
            });;


});

// GET MERCHANT GROUP BY
router.get('/groupby=:groupby/page=:page/limit=:limit/sort=:sort', (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    let groupby = req.params.groupby;

    db.select('city')
        .from('merchant')
        .where('activated', 1)
        .groupBy(groupby)
        .orderBy(groupby, sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            // paginator.data[0].coordinat2 = JSON.parse(paginator.data[0].coordinat2)
            // console.log(paginator)
            res.json({
                success: true,
                message: "sukses ambil data merchant group by",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: sortBy,
                data: paginator.data,
            });
        });


});

router.post('/scan/voucher/fid_merchant=:fid_merchant/id=:id', (req, res) =>  {
    let details = {
        used: 1,
        used_date: date.utc()
    }
    let fid_user = req.params.id.substring(0, req.params.id.length-7)
    let voucher_code = req.params.id.substring(req.params.id.length-7, req.params.id.length)
    let fid_merchant = req.params.fid_merchant
    // console.log(fid_user, voucher_code, fid_merchant)

    db.select(
        't1.id',
        't1.quantity',
        't2.promo_title',
        'voucher_code',
        't1.used',
        't1.expired_voucher',
    )
        .from('ecommerce_transaction as t1')
        .leftJoin('merchant_promo as t2', 't1.fid_promo', 't2.id')
        .where('t1.fid_user', fid_user)
        .andWhere('t1.voucher_code', voucher_code)
        // .andWhere('used', 0)
        .andWhere('t1.fid_merchant', fid_merchant)
        .then((data) => {
            // console.log(moment.utc().format('YYYY-MM-DD'), moment(data[0].expired_voucher).format('YYYY-MM-DD'))
            // console.log(data.length)
            // console.log(moment(moment.utc().format('YYYY-MM-DD')).isSameOrBefore(moment(data[0].expired_voucher).format('YYYY-MM-DD'), 'day')); // false)

            if (data.length > 0){
                if(data[0].used === 0){
                    if (moment(moment.utc().format('YYYY-MM-DD')).isSameOrBefore(moment(data[0].expired_voucher).format('YYYY-MM-DD'), 'day')){
                        db('ecommerce_transaction')
                            .where('fid_user', req.params.id.substring(0, req.params.id.length-7))
                            .andWhere('voucher_code', req.params.id.substring(req.params.id.length-7, req.params.id.length))
                            .update(details)
                            .then(data2 => {
                                if(data2 > 0){
                                    res.json({
                                        success: true,
                                        message: "Voucher berhasil digunakan",
                                        // count: data.length,
                                        data: data,
                                    });
                                }
                            })
                            .catch((err) =>{
                                console.log(err)
                                res.json({
                                    success: false,
                                    message: "Update Voucher failed.",
                                    // count: data.length,
                                    data: err,
                                });
                            });
                    }else{
                        res.json({
                            success: false,
                            message: "Voucher telah expired",
                            // count: data.length,
                            // data: data,
                        });
                    }

                }else{
                    res.json({
                        success: false,
                        message: "Voucher telah digunakan",
                        // count: data.length,
                        // data: data,
                    });
                }
            }else{
                res.json({
                    success: false,
                    message: "Merchant tidak sesuai atau voucher tidak di temukan",
                    // count: data.length,
                    data: data,
                });
            }
        })
});

// ------------------------------------- SAMPLE CODE

// TEST BARCODE
router.get('/barcode/text=:text', (req, res) => {
    let text = req.params.text
    // globalFunc.generateBarcodeWithLogo(text)
    globalFunc.generateBarcode(text)
    res.json({
        success: true,
        message: "sukses test barcode merchant",
    });
});


module.exports = router;