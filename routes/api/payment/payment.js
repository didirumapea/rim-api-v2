const express = require('express');
const router = express.Router();
const db = require('../../../database').db_rim; // as const knex = require('knex')(config);
const moment = require('moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const checkAuth = require('../../../middleware/check-auth')
const mailing = require('../../../plugins/mailing')
const pdfGen = require('../../../plugins/pdf-generator')
const payment = require('../../../plugins/payment')
const date = require('../../../config/moment-date-format')
const config = require('../../../config/config')
const randomstring = require("randomstring");
const midtransClient = require('midtrans-client')
const dotenv = require('dotenv');
dotenv.config();
const Timeout = require('smart-timeout');

// console.log(process.env.NODE_ENV)
// Create Core API instance
// let coreApi = new midtransClient.CoreApi({
//     isProduction : false,
//     serverKey : 'SB-Mid-server-KgVg_1tfaMXuPInPsOF_yw8L',
//     clientKey : 'SB-Mid-client-vMIhgH834cfQjCut'
// });
let serverKey = config.developmentMidtrans.serverKey
let clientKey = config.developmentMidtrans.clientKey
let isProd = false
if(process.env.NODE_ENV === 'production'){
    isProd = true
    serverKey = config.productionMidtrans.serverKey
    clientKey = config.productionMidtrans.clientKey
}

let apiClient = new midtransClient.Snap({
    isProduction : isProd,
    serverKey : serverKey,
    clientKey : clientKey
});

let snap = new midtransClient.Snap({
    isProduction : isProd,
    serverKey : serverKey,
    clientKey : clientKey
});


// let dateNow = moment().format("YYYY-MM-DD hh:mm:ss")

router.get('/', (req, res) => {
    // console.log(req.query)
    res.send('We Are In PAYMENT Route')
})

// ORDER VOUCHER MIDTRANS
router.post('/order-voucher', checkAuth, (req, res) => {
    // console.log(req.userData)
    let momentUnix = moment().unix()
    // let transId = null
    console.log(date.utc())
    let randomInvoice = randomstring.generate({
        length: 9,
        capitalization: 'uppercase',
        charset: 'hex'
    });
    let randomVoucherCode = randomstring.generate({
        length: 4,
        capitalization: 'uppercase',
        charset: 'hex'
    });

    let invoice = 'PV'+randomInvoice
    let voucherCode = randomVoucherCode + momentUnix.toString().substr(momentUnix.toString().length - 3, 3)
    let trans_status = 'pending';

    let paymentDetail = {
        inv_code: invoice,
        fid_promo: req.body.fid_promo,
        fid_user: req.userData.id,
        voucher_code: voucherCode, // kode voucher untuk di redeem di outlet2 yg di pilih
        voucher_promo_code: req.body.voucher_promo_code, // voucher diskon
        rtc_unique_code: req.body.rtc_unique_code,
        transaction_status: trans_status,
        fid_merchant: req.body.fid_merchant,
        quantity: req.body.quantity,
        tax: req.body.tax,
        total: req.body.total, //
        cashback: req.body.cashback, // cashback yg di dapat dari membeli voucher
        cashback_used: req.body.cashbackUsed, // cashback yg di gunakana untuk membeli voucher ini
        tz: req.body.tz,
        created: date.utc(),
        expired_voucher: date.getUtcTimeWithAddDays(req.body.expired)
    }
    console.log(paymentDetail)
    db('ecommerce_transaction')
        .insert(paymentDetail)
        .then(data => {
            db.select(
                't3.fullname',
                't3.email',
                't3.phone',
                't2.promo_title as promo_name',
                't2.promo_price',
                't1.quantity',
                't1.inv_code',
                // 't1.tax',
                // 't1.payment_type',
                // 't1.total',
                // 't1.created as tgl_pembelian'
            )
                .from('ecommerce_transaction as t1')
                .innerJoin('merchant_promo as t2', 't1.fid_promo', 't2.id')
                .innerJoin('members as t3', 't1.fid_user', 't3.id')
                .where('t1.id', data[0])
                // .orderBy('position', sortBy)
                // .paginate(limit, page, true)
                .then(data2 => {
                    if (data.length > 0){
                    // console.log(data2[0].promo_price, data2[0].quantity, paymentDetail.cashback_used)
                        let parameter = {
                            "transaction_details": {
                                "order_id": paymentDetail.inv_code,
                                "gross_amount": (data2[0].promo_price * data2[0].quantity) - req.body.code_discount_price - paymentDetail.cashback_used //paymentDetail.total
                                // "gross_amount": (data2[0].promo_price * data2[0].quantity) - paymentDetail.cashback_used //paymentDetail.total
                            },
                            "credit_card": {
                                "secure": true
                            },
                            "item_details": [
                                {
                                    "name": data2[0].promo_name.substring(0, 47)+'...',
                                    "price": data2[0].promo_price,
                                    "quantity": data2[0].quantity,
                                },
                            ],
                            "customer_details": {
                                "first_name": data2[0].fullname,
                                "email": data2[0].email,
                                "phone": data2[0].phone,
                            },
                        }
                        // console.log(paymentDetail.cashback_used)
                        // IF CASHBACK USER IS USED
                        if (paymentDetail.cashback_used !== '0'){
                            // parameter.item_details.splice(0, 0, {
                            //     "name": 'Cashback points used',
                            //     "price": '-'+paymentDetail.cashback_used,
                            //     "quantity": 1,
                            // });
                            parameter.item_details.push({
                                "name": 'Cashback points used',
                                "price": '-'+paymentDetail.cashback_used,
                                "quantity": 1,
                            })
                        }
                        // IF VOUCHER CODE PROMO USED
                        if (req.body.code_discount_price !== 0 || req.body.code_discount_price !== '0'){
                            parameter.item_details.push({
                                "name": `Voucher Code ${paymentDetail.voucher_promo_code} apply`,
                                "price": '-'+req.body.code_discount_price,
                                "quantity": 1,
                            })
                        }
                        snap.createTransaction(parameter)
                            .then((transaction)=>{
                                // transaction redirect_url
                                let redirectUrl = transaction.redirect_url;
                                console.log('redirectUrl:',redirectUrl);
                                // console.log('transaction:',transaction);
                                data2[0].redirect_url = redirectUrl
                                // UPDATE REDIRECT URL
                                db('ecommerce_transaction')
                                    .where('id', data[0])
                                    .update({redirect_url: redirectUrl})
                                    .then(data3 => {
                                        // mailing.sendEmailPending(data2[0], mailerData.template, mailerData.subject, mailerData.from)
                                        // console.log(data3)
                                        res.json({
                                            success: true,
                                            message: "Create order voucher success",
                                            data2: data2,
                                            data: data,
                                            data3: data3
                                            // redirect_url: redirectUrl
                                        })

                                    })
                                    .catch((err) =>{
                                    console.log(err)
                                    res.json({
                                        success: false,
                                        message: "Update redirect url failed.",
                                        // count: data.length,
                                        data: err,
                                    });
                                });
                                // console.log(parameter)
                                // console.log('transaction:',transaction);
                            })

                    }else{
                        res.json({
                            success: false,
                            message: "Create order voucher failed",
                            // data: data,
                        })
                    }
                    // console.log(data)
                })
        })
        .catch((err) => {
            console.log(err)
        });

});

// ORDER VOUCHER DIRECT
router.post('/order-voucher-direct', checkAuth, (req, res) => {
    // console.log(req.userData)
    let momentUnix = moment().unix()
    // let transId = null
    // let paidDate = null
    let randomInvoice = randomstring.generate({
        length: 9,
        capitalization: 'uppercase',
        charset: 'hex'
    });
    let randomVoucherCode = randomstring.generate({
        length: 4,
        capitalization: 'uppercase',
        charset: 'hex'
    });

    let invoice = 'PV'+randomInvoice
    let voucherCode = randomVoucherCode + momentUnix.toString().substr(momentUnix.toString().length - 3, 3)
    let trans_status = 'unverification';

    let paymentDetail = {
        inv_code: invoice,
        fid_promo: req.body.fid_promo,
        fid_user: req.userData.id,
        voucher_code: voucherCode, // kode voucher untuk di redeem di outlet2 yg di pilih
        voucher_promo_code: req.body.voucher_promo_code, // voucher diskon
        rtc_unique_code: req.body.rtc_unique_code,
        transaction_status: trans_status,
        payment_type: 'direct_transfer',
        fid_merchant: req.body.fid_merchant,
        quantity: req.body.quantity,
        tax: req.body.tax,
        total: req.body.total, // total sudah include dengan penggunaan poin cashback
        cashback: req.body.cashback, // cashback yg di dapat dari membeli voucher
        cashback_used: req.body.cashbackUsed, // cashback yg di gunakan untuk membeli voucher ini
        tz: req.body.tz,
        created: date.utc(),
        expired_voucher: date.getUtcTimeWithAddDays(req.body.expired),
        //
        bank_name: req.body.bank_name,
        bank_account_name: req.body.bank_account_name,
        bank_account_no: req.body.bank_account_no
    }
    // console.log(paymentDetail)
    db('ecommerce_transaction')
        .insert(paymentDetail)
        .then(data => {
            db.select(
                't3.fullname',
                't3.email',
                't3.phone',
                't2.promo_title as promo_name',
                't2.promo_price',
                't1.quantity',
                't1.inv_code',
            )
                .from('ecommerce_transaction as t1')
                .innerJoin('merchant_promo as t2', 't1.fid_promo', 't2.id')
                .innerJoin('members as t3', 't1.fid_user', 't3.id')
                .where('t1.id', data[0])
                // .orderBy('position', sortBy)
                // .paginate(limit, page, true)
                .then(data2 => {
                    if (data.length > 0){
                        // console.log(data2[0].inv_code, data2[0].quantity)
                        //
                        payment.transPending(data2[0].inv_code)
                        // SET TIMEOUT 1 HOUR PAYMENT DIRECT
                        Timeout.set(data2[0].inv_code, payment.transSettleDirect, 3600000, data2[0].inv_code)
                        console.log(`Timeout set for 1 hour with inv Code ${data2[0].inv_code}`)
                        // setTimeout(() => {
                        //     transUnverification(data2[0].inv_code)
                        //     // console.log('timeout beyond time');
                        // }, 3600000); // 1 hours = 3600000
                        res.json({
                            success: true,
                            message: "Create order voucher with direct success",
                            data2: data2,
                            data: data,
                            // redirect_url: redirectUrl
                        })

                    }else{
                        res.json({
                            success: false,
                            message: "Create order voucher with direct failed",
                            // data: data,
                        })
                    }
                    // console.log(data)
                })
        })
        .catch((err) => {
            console.log(err)
        });

});

// ORDER VOUCHER DIRECT
router.post('/order-voucher-direct-with-bca-va-number', checkAuth, (req, res) => {
    // console.log(req.userData)
    let momentUnix = moment().unix()
    // let transId = null
    // let paidDate = null
    let randomInvoice = randomstring.generate({
        length: 9,
        capitalization: 'uppercase',
        charset: 'hex'
    });
    let randomVoucherCode = randomstring.generate({
        length: 4,
        capitalization: 'uppercase',
        charset: 'hex'
    });
    let randomBcaVaNumber = randomstring.generate({
        length: 4,
        charset: 'numeric'
    });

    let va_number = req.body.fid_promo + req.body.fid_merchant + randomBcaVaNumber.toString() + req.userData.id
    let invoice = 'PV'+randomInvoice
    let voucherCode = randomVoucherCode + momentUnix.toString().substr(momentUnix.toString().length - 3, 3)
    let trans_status = 'unverification';

    let paymentDetail = {
        inv_code: invoice,
        fid_promo: req.body.fid_promo,
        fid_user: req.userData.id,
        voucher_code: voucherCode, // kode voucher untuk di redeem di outlet2 yg di pilih
        voucher_promo_code: req.body.voucher_promo_code, // voucher diskon
        rtc_unique_code: req.body.rtc_unique_code,
        transaction_status: trans_status,
        payment_type: 'direct_transfer',
        fid_merchant: req.body.fid_merchant,
        quantity: req.body.quantity,
        tax: req.body.tax,
        total: req.body.total, // total sudah include dengan penggunaan poin cashback
        cashback: req.body.cashback, // cashback yg di dapat dari membeli voucher
        cashback_used: req.body.cashbackUsed, // cashback yg di gunakan untuk membeli voucher ini
        tz: req.body.tz,
        created: date.utc(),
        expired_voucher: date.getUtcTimeWithAddDays(req.body.expired),
        //
        bank_name: req.body.bank_name,
        va_number: va_number
    }
    // console.log(paymentDetail)
    db('ecommerce_transaction')
        .insert(paymentDetail)
        .then(data => {
            db.select(
                't3.fullname',
                't3.email',
                't3.phone',
                't2.promo_title as promo_name',
                't2.promo_price',
                't1.quantity',
                't1.inv_code',
            )
                .from('ecommerce_transaction as t1')
                .innerJoin('merchant_promo as t2', 't1.fid_promo', 't2.id')
                .innerJoin('members as t3', 't1.fid_user', 't3.id')
                .where('t1.id', data[0])
                // .orderBy('position', sortBy)
                // .paginate(limit, page, true)
                .then(data2 => {
                    if (data.length > 0){
                        // console.log(data2[0].inv_code, data2[0].quantity)
                        //
                        payment.transPending(data2[0].inv_code)
                        // SET TIMEOUT 1 HOUR PAYMENT DIRECT
                        Timeout.set(data2[0].inv_code, payment.transSettleDirect, 3600000, data2[0].inv_code)
                        console.log(`Timeout set for 1 hour with inv Code ${data2[0].inv_code}`)
                        // setTimeout(() => {
                        //     transUnverification(data2[0].inv_code)
                        //     // console.log('timeout beyond time');
                        // }, 3600000); // 1 hours = 3600000
                        res.json({
                            success: true,
                            message: "Create order voucher with direct success",
                            data2: data2,
                            data: data,
                            // redirect_url: redirectUrl
                        })

                    }else{
                        res.json({
                            success: false,
                            message: "Create order voucher with direct failed",
                            // data: data,
                        })
                    }
                    // console.log(data)
                })
        })
        .catch((err) => {
            console.log(err)
        });

});

//
router.get('/clear-timeout-direct-settle/inv_code=:inv_code', (req, res) =>   {
    let inv_code = req.params.inv_code

    if (Timeout.exists(inv_code)){
        Timeout.clear(inv_code, erase = true)
        console.log('Timeout Cleared for Inv Code "+inv_code')
        res.json({
            success: true,
            message: "Timeout Cleared for Inv Code "+inv_code,
            data: 'data',
        });
    }else{
        console.log(`Clear Timeout failed, Inv Code ${inv_code} not found`)
        res.json({
            success: true,
            message: `Clear Timeout failed, Inv Code ${inv_code} not found`,
            data: 'data',
        });
    }
});

// ORDER VOUCHER ZERO VALUES
router.post('/order-voucher-zero-value', checkAuth, (req, res) => {
    // console.log(req.userData)
    let momentUnix = moment().unix()
    let labelClasess = 'success';
    // let transId = null
    // let paidDate = null
    let randomInvoice = randomstring.generate({
        length: 9,
        capitalization: 'uppercase',
        charset: 'hex'
    });
    let randomVoucherCode = randomstring.generate({
        length: 4,
        capitalization: 'uppercase',
        charset: 'hex'
    });

    let invoice = 'PV'+randomInvoice
    let voucherCode = randomVoucherCode + momentUnix.toString().substr(momentUnix.toString().length - 3, 3)
    let trans_status = 'settlement';

    let paymentDetail = {
        inv_code: invoice,
        fid_promo: req.body.fid_promo,
        fid_user: req.userData.id,
        voucher_code: voucherCode, // kode voucher untuk di redeem di outlet2 yg di pilih
        voucher_promo_code: req.body.voucher_promo_code, // voucher diskon
        rtc_unique_code: req.body.rtc_unique_code,
        transaction_status: trans_status,
        payment_type: 'point_cashback',
        fid_merchant: req.body.fid_merchant,
        quantity: req.body.quantity,
        tax: req.body.tax,
        total: req.body.total, // total sudah include dengan penggunaan poin cashback
        cashback: req.body.cashback, // cashback yg di dapat dari membeli voucher
        cashback_used: req.body.cashbackUsed, // cashback yg di gunakan untuk membeli voucher ini
        tz: req.body.tz,
        created: date.utc(),
        paid_date: date.utc(),
        expired_voucher: date.getUtcTimeWithAddDays(req.body.expired),
    }
    // console.log(paymentDetail)
    db('ecommerce_transaction')
        .insert(paymentDetail)
        .then(data => {
            db.select(
                't3.fullname',
                't3.email',
                't3.phone',
                't2.promo_title as promo_name',
                't2.promo_price',
                't1.quantity',
                't1.inv_code',
            )
                .from('ecommerce_transaction as t1')
                .innerJoin('merchant_promo as t2', 't1.fid_promo', 't2.id')
                .innerJoin('members as t3', 't1.fid_user', 't3.id')
                .where('t1.id', data[0])
                // .orderBy('position', sortBy)
                // .paginate(limit, page, true)
                .then(data2 => {
                    if (data.length > 0){
                        // console.log(data2[0].basic_price, data2[0].quantity)
                        // console.log(paymentDetail.cashback_used)
                        //
                        payment.transSettle(data2[0].inv_code)
                        res.json({
                            success: true,
                            message: "Create order voucher zero value success",
                            data2: data2,
                            data: data,
                            // redirect_url: redirectUrl
                        })
                    }else{
                        res.json({
                            success: false,
                            message: "Create order voucher zero value failed",
                            // data: data,
                        })
                    }
                    // console.log(data)
                })
        })
        .catch((err) => {
            console.log(err)
        });

});

// LIVE UPDATE MIDTRANS
router.post('/notification',  (req, res) => {
    // console.log(date.getTimezoneOffset())
    let postMidtransbody = {
        transaction_id: req.body.transaction_id, // for comparing with midtrans
    }
    // console.log('=========================== BODY FROM MIDTRANS ==========================')
    // console.log(req.body)
    // console.log('=========================== END BODY FROM MIDTRANS ==========================')

    apiClient.transaction.notification(postMidtransbody)
        .then((statusResponse)=>{
            switch (statusResponse.payment_type){
                case 'bank_transfer':
                    console.log(statusResponse)
                    if (statusResponse.va_numbers !== undefined){
                        if (statusResponse.va_numbers.length > 0){
                            postMidtransbody.bank_name = statusResponse.va_numbers[0].bank
                            postMidtransbody.va_number = statusResponse.va_numbers[0].va_number
                        }
                    }else{
                        postMidtransbody.bank_name = 'permata'
                        postMidtransbody.va_number = statusResponse.permata_va_number
                    }

                    break
                case 'gopay':
                    break
                case 'credit_card':
                    break
                case 'echannel':
                    postMidtransbody.bank_name = statusResponse.biller_code
                    postMidtransbody.va_number = statusResponse.bill_key
                    break
            }
            // let orderId = statusResponse.order_id;
            let transactionStatus = statusResponse.transaction_status;
            let fraudStatus = statusResponse.fraud_status;
            postMidtransbody.transaction_status = transactionStatus
            postMidtransbody.signature_key = statusResponse.signature_key
            postMidtransbody.fraud_status = fraudStatus
            postMidtransbody.status_code = statusResponse.status_code
            postMidtransbody.payment_type = statusResponse.payment_type
            postMidtransbody.inv_code = statusResponse.order_id

            // console.log(`Transaction notification received. Order ID: ${orderId}. Transaction status: ${transactionStatus}. Fraud status: ${fraudStatus}`);
            // console.log('=========================== MIDTRANS NOTIFICATION STATUS ==========================')
            // console.log(statusResponse)
            // console.log('=========================== END MIDTRANS NOTIFICATION STATUS ==========================')
            // Sample transactionStatus handling logic

            if (transactionStatus === 'capture'){
                if (fraudStatus === 'challenge'){
                    // TODO set transaction status on your databaase to 'challenge'
                    postMidtransbody.transaction_status = 'settlement'
                    postMidtransbody.fraud_status = fraudStatus
                    postMidtransbody.modified = statusResponse.transaction_time
                } else if (fraudStatus === 'accept'){
                    // TODO set transaction status on your databaase to 'success'
                    postMidtransbody.transaction_status = 'settlement'
                    postMidtransbody.fraud_status = fraudStatus
                    postMidtransbody.modified = statusResponse.transaction_time
                }
            } else if (transactionStatus === 'cancel' ||
                transactionStatus === 'deny' ||
                transactionStatus === 'expire'){
                // TODO set transaction status on your databaase to 'failure'
                payment.transFailure(postMidtransbody.inv_code)
                postMidtransbody.transaction_status = 'failure'
                postMidtransbody.modified = statusResponse.transaction_time
            } else if (transactionStatus === 'pending'){
                // console.log(postMidtransbody.inv_code)
                payment.transPending(postMidtransbody.inv_code)
                // TODO set transaction status on your databaase to 'pending' / waiting payment
                postMidtransbody.transaction_status = transactionStatus
                postMidtransbody.modified = statusResponse.transaction_time
            } else if (transactionStatus === 'settlement'){
                payment.transSettle(postMidtransbody.inv_code, statusResponse.settlement_time)
                // TODO set transaction status on your databaase to 'settlement'
                postMidtransbody.transaction_status = transactionStatus
                postMidtransbody.paid_date = statusResponse.settlement_time
            }
            // console.log(postMidtransbody, statusResponse.order_id)
            // UPDATE REDIRECT URL
            db('ecommerce_transaction')
                .where('inv_code', statusResponse.order_id)
                .update(postMidtransbody)
                .then(data => {
                    // console.log(data3)
                    res.json({
                        success: true,
                        message: "Update midtrans notification success",
                        data: data,
                        statusResponse: statusResponse
                        // redirect_url: redirectUrl
                    })

                }).catch((err) =>{
                console.log(err)
                res.json({
                    success: false,
                    message: "Update midtrans notification failed.",
                    // count: data.length,
                    data: err,
                });
            });
        });
});
// MANUAL UPDATE MIDTRANS
router.post('/manual-notification',  (req, res) => {

    let postMidtransbody = {
        transaction_id: req.body.transaction_id, // for comparing with midtrans
        transaction_status: req.body.transaction_status,
        signature_key: req.body.signature_key,
        fraud_status: req.body.fraud_status,
        status_code: req.body.status_code,
        payment_type: req.body.payment_type,
        inv_code: req.body.order_id, // for comparing with database local
    }
    console.log(req.body)

    apiClient.transaction.notification(postMidtransbody)
        .then((statusResponse)=>{
            let orderId = statusResponse.order_id;
            let transactionStatus = statusResponse.transaction_status;
            let fraudStatus = statusResponse.fraud_status;

            console.log(`Transaction notification received. Order ID: ${orderId}. Transaction status: ${transactionStatus}. Fraud status: ${fraudStatus}`);

            // Sample transactionStatus handling logic

            if (transactionStatus === 'capture'){
                postMidtransbody.transaction_status = transactionStatus
                if (fraudStatus === 'challenge'){
                    // TODO set transaction status on your databaase to 'challenge'
                    postMidtransbody.fraud_status = fraudStatus
                    postMidtransbody.modified = req.body.transaction_time
                } else if (fraudStatus === 'accept'){
                    // TODO set transaction status on your databaase to 'success'
                    postMidtransbody.fraud_status = fraudStatus
                    postMidtransbody.modified = req.body.transaction_time
                }
            } else if (transactionStatus === 'cancel' ||
                transactionStatus === 'deny' ||
                transactionStatus === 'expire'){
                // TODO set transaction status on your databaase to 'failure'
                postMidtransbody.transaction_status = transactionStatus
                postMidtransbody.modified = req.body.transaction_time
            } else if (transactionStatus === 'pending'){
                // TODO set transaction status on your databaase to 'pending' / waiting payment
                postMidtransbody.transaction_status = transactionStatus
                postMidtransbody.modified = req.body.transaction_time
            } else if (transactionStatus === 'settlement'){
                // TODO set transaction status on your databaase to 'settlement'
                postMidtransbody.transaction_status = transactionStatus
                postMidtransbody.paid_date = req.body.transaction_time
            }
            console.log(postMidtransbody)
            // UPDATE REDIRECT URL
            db('ecommerce_transaction')
                .where('inv_code', orderId)
                .update(postMidtransbody)
                .then(data => {
                    // console.log(data3)
                    res.json({
                        success: true,
                        message: "Update midtrans notification success",
                        data: data,
                        // redirect_url: redirectUrl
                    })

                }).catch((err) =>{
                console.log(err)
                res.json({
                    success: false,
                    message: "Update midtrans notification failed.",
                    // count: data.length,
                    data: err,
                });
            });
        });
});

// MIDTRANS GET STATUS PAYMENT BASE ON ORDER ID
router.get('/status-payment/payment-type=:payment_type/order_id=:order_id', checkAuth, (req, res) => {
    // console.log('PWD-'+moment().format('X'))
    let payment_type = req.params.payment_type
    let order_id = req.params.order_id
    switch (payment_type){
        case 'midtrans':
            apiClient.transaction.status(order_id)
                .then((response)=>{
                    res.json({
                        success: true,
                        message: "Get order "+order_id+" details success",
                        data: response
                        // token: token
                    });
                    // console.log(response)
                    // do something to `response` object
                })
                .catch((err) => {
                    res.json({
                        success: false,
                        message: "Get order "+order_id+" details failed or not found",
                        // data: response
                        // token: token
                    });
                });
            break;
        case 'direct':
            db.select(
                'id',
                    'transaction_status'
            )
                .from('ecommerce_transaction')
                .where('inv_code', order_id)
                .then(data => {
                    if (data.length > 0){
                        res.json({
                            success: true,
                            message: "Get order "+order_id+" details success",
                            data: data[0],
                        })
                    }else{
                        res.json({
                            success: false,
                            message: "Get order "+order_id+" details success",
                            // data: data,
                        })
                    }
                    // console.log(data)
                })
            break;
        default:
            res.json({
                success: false,
                message: "no one selected payment type",
                // data: response
                // token: token
            });
            break;
    }


});

// MIDTRANS GET STATUS PAYMENT B2B BASE ON ORDER ID
router.get('/midtrans-status-payment/b2b/order_id=:order_id',  (req, res) => {
    let order_id = req.params.order_id

    apiClient.transaction.status(order_id)
        .then((response)=>{
            console.log(response)
            // do something to `response` object
        });
    res.json({
        success: true,
        message: "Add payment voucher success",
        // data: data
        // token: token
    });

});

router.get('/detail-payment',  (req, res) => {
    // let page = req.params.page;
    let email = req.params.email
    // let password = req.body.password
    // console.log(req.params)

    db.select(
        'id',
        'fullname',
        'email',
        'password'
    )
        .from('members as t1')
        .where('email', '=', email)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length > 0){
                res.json({
                    success: true,
                    message: "Email terdaftar",
                    // data: data,
                })
            }else{
                res.json({
                    success: false,
                    message: "Email salah atau belum terdaftar",
                    // data: data,
                })
            }
            // console.log(data)
        })

});

// MIDTRANS SAMPLE
router.get('/midtrans/inv_code=:inv_code',  (req, res) => {
    let inv_code = req.params.inv_code

    setTimeout(() => {
        payment.transUnverification(inv_code)
        // console.log('timeout beyond time');
    }, 5000);

    let parameter = {
        "transaction_details": {
            "order_id": "PV3210DJSKAJIW",
            "gross_amount": 3000000
        },
        "credit_card":{
            "secure" : true
        },
        "item_details": [
            {
                "name" : 'Point Cashback Used',
                "price" : '0',
                "quantity": 1,
            },
            {
                "name" : 'Voucher Rabbit',
                "price" : '500000',
                "quantity": 3,
            },
            {
                "name" : 'Voucher Rabbit 3',
                "price" : '300000',
                "quantity": 1,
            },
            {
                "name" : 'Voucher Rabbit 6',
                "price" : '400000',
                "quantity": 1,
            },
            {
                "name" : 'Voucher Rabbit 2',
                "price" : '700000',
                "quantity": 1,
            },
            {
                "name" : 'Voucher Rabbit 4',
                "price" : '100000',
                "quantity": 1,
            },
        ],
        "customer_details": {
            "first_name": 'Didi Kurnia Rumapea',
            "email": 'didi@rimember.id',
            "phone": '0812-993-9219',
        },

    };

    snap.createTransaction(parameter)
        .then((transaction)=>{
            // transaction redirect_url
            let redirectUrl = transaction.redirect_url;
            // console.log('redirectUrl:',redirectUrl);
            // console.log('transaction:',transaction);
        })


    res.json({
        success: true,
        message: "Add payment voucher success",
        // data: data
        // token: token
    });

});
//
router.post('/finish',  (req, res) => {
    // Create Core API instance
    let coreApi = new midtransClient.CoreApi({
        isProduction : false,
        serverKey : 'SB-Mid-server-KgVg_1tfaMXuPInPsOF_yw8L',
        clientKey : 'SB-Mid-client-vMIhgH834cfQjCut'
    });

    let snap = new midtransClient.Snap({
        isProduction : false,
        serverKey : 'SB-Mid-server-KgVg_1tfaMXuPInPsOF_yw8L',
        clientKey : 'SB-Mid-client-vMIhgH834cfQjCut'
    });

    let parameter = {
        "transaction_details": {
            "order_id": "test-transaction-121113321",
            "gross_amount": 2000000
        },
        "credit_card":{
            "secure" : true
        },
        "item_details": [
            {
                "name" : 'Voucher Rabbit',
                "price" : '500000',
                "quantity": 1,
            },
            {
                "name" : 'Voucher Rabbit 3',
                "price" : '300000',
                "quantity": 1,
            },
            {
                "name" : 'Voucher Rabbit 6',
                "price" : '400000',
                "quantity": 1,
            },
            {
                "name" : 'Voucher Rabbit 2',
                "price" : '700000',
                "quantity": 1,
            },
            {
                "name" : 'Voucher Rabbit 4',
                "price" : '100000',
                "quantity": 1,
            },
        ],
        "customer_details": {
            "first_name": 'Didi Kurnia Rumapea',
            "email": 'didi@rimember.id',
            "phone": '0812-993-9219',
        },

    };




    snap.createTransaction(parameter)
        .then((transaction)=>{
            // transaction token
            let transactionToken = transaction.token;
            console.log('transactionToken:',transactionToken);
        })

    snap.createTransaction(parameter)
        .then((transaction)=>{
            // transaction redirect_url
            let redirectUrl = transaction.redirect_url;
            console.log('redirectUrl:',redirectUrl);
            console.log('redirectUrl:',transaction);
        })



    // console.log(date.getTimeWithAddDays(46))
    // let momentUnix = moment().unix()
    // let labelClasess = 'grey';
    // // let transId = null
    // // let paidDate = null
    // let randomInvoice = randomstring.generate({
    //     length: 9,
    //     capitalization: 'uppercase',
    //     charset: 'hex'
    // });
    // let randomVoucherCode = randomstring.generate({
    //     length: 4,
    //     capitalization: 'uppercase',
    //     charset: 'hex'
    // });
    //
    // let invoice = 'PV'+randomInvoice
    // let voucherCode = randomVoucherCode + momentUnix.toString().substr(momentUnix.toString().length - 3, 3)
    // let trans_status = 'pending';
    //
    // let paymentDetail = {
    //     inv_code: invoice,
    //     fid_promo: req.body.fid_promo,
    //     fid_user: req.body.fid_user,
    //     voucher_code: voucherCode, // kode voucher untuk di redeem di outlet2 yg di pilih
    //     voucher_promo_code: req.body.voucher_promo, // voucher diskon
    //     rtc_unique_code: req.body.rtc_unique_code,
    //     transaction_status: trans_status,
    //     fid_merchant: req.body.fid_merchant,
    //     quantity: req.body.quantity,
    //     tax: req.body.tax,
    //     total: req.body.total, //
    //     cashback: req.body.cashback, // cashback yg di dapat dari membeli voucher
    //     cashback_used: req.body.cashbackUsed, // cashback yg di gunakana untuk membeli voucher ini
    //     tz: req.body.tz,
    //     created: date.utc(),
    //     expired_voucher: date.getUtcTimeWithAddDays(req.body.expired)
    // }
    //
    // db('ecommerce_transaction')
    //     .insert(paymentDetail)
    //     .then(data => {
    res.json({
        success: true,
        message: "Add payment voucher success",
        // data: data
        // token: token
    });
    //     })
    //     .catch((err) => {
    //         console.log(err)
    //     });

});

module.exports = router;

