const express = require('express');
const router = express.Router();
const db = require('../../../database').db_rim; // as const knex = require('knex')(config);
const moment = require('moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const date = require('../../../config/moment-date-format')
const splitImage = require('../../../config/global-function').splitImage
// const autocorrect = require('autocorrect')()


let dateNow = moment().format("YYYY-MM-DD hh:mm:ss")

router.get('/', (req, res) => {
    // console.log(req.query)
    res.send('We Are In Produk Route')
})


// router.get('/catId=:catId/page=:page/limit=:limit/sort=:sort/min=:min/max=:max/rating=:rating',  (req, res) => {
router.get('/:slug',  (req, res) => {
    let slug = req.params.slug
    // console.log(catId, page, limit, sort, min, max, rating)
    // console.log(req)
    db.select(
        't1.id',
        't1.fid_merchant',
        't2.default_image as merchant_image',
        't3.title as category',
        't3.slug as category_slug',
        't3.id as category_id',
        't2.title as merchant_name',
        't2.address as merchant_address',
        't2.location as merchant_location',
        't2.city as merchant_city',
        'promo_title',
        't1.slug',
        'promo_desc',
        'benefit',
        'basic_price',
        'promo_price',
        't1.cashback_percentage',
        't1.max_cashback',
        't1.start_date',
        't1.end_date',
        'tnc',
        'expired',
        'images',
        'quantity',
        'soldout',
    )
        .from('merchant_promo as t1')
        .innerJoin('merchant as t2', 't2.id', 't1.fid_merchant')
        .innerJoin('merchant_category as t3','t2.fid_category', 't3.id')
        .andWhere('t1.activated',1)
        .andWhere('quantity', '>', 0)
        // .andWhere('t1.fid_account_type', 1)
        // .andWhere('t2.city', 'Bandung')
        .andWhere('t1.slug', slug)
        .andWhere('end_date', '>', date.localDate())
        // .orderBy('t1.id', sort)
        // .paginate(limit, page, true)
        .then(data => {
            // console.log(paginator)
            if (data.length !== 0){
                // console.log(data)
                let bigImage = []
                let thumbImage = []
                // let tncSpl = splitImage.splitTnc(data[0].benefit, data[0].tnc)
                // data[0].benefit = tncSpl.objBenefit
                // data[0].tnc = tncSpl.objTnc
                // console.log(tncSpl)
                data.forEach((element1, index1) => {
                    element1.images.split(',').forEach((element2, index2) => {
                        if (element2.startsWith('big')){
                            // thumbImage['img']
                            bigImage.push(element2)
                        }else{
                            thumbImage.push(element2)
                        }
                        // console.log(index+'. '+element)
                        // img['img'+index] = element
                    })
                    data[index1].images = {thumbImage: thumbImage, bigImage: bigImage}
                    thumbImage = []
                    bigImage = []
                })
                res.json({
                    success: true,
                    message: 'Sukses ambil data',
                    // count: paginator.total,
                    // current_page: paginator.current_page,
                    // last_page: paginator.last_page,
                    // limit: paginator.per_page,
                    // // sortBy: sortBy,
                    data: data,
                    // data2: tncSpl,
                });
            }else{
                res.json({
                    // name: data
                    success: true,
                    message: "Data Kosong",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }
        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'something error check your server logs.',
                data: err.toLocaleString()
            });
            console.log(err)
        });

});

// GET PRODUCT WEB
router.get('/web/:slug',  (req, res) => {
    let slug = req.params.slug
    // console.log(catId, page, limit, sort, min, max, rating)
    // console.log(req)
    db.select(
        't1.id',
        't1.fid_merchant',
        't2.default_image as merchant_image',
        't3.title as category',
        't3.id as category_id',
        't3.slug as category_slug',
        't2.title as merchant_name',
        't2.address as merchant_address',
        't2.location as merchant_location',
        't2.city as merchant_city',
        'promo_title',
        't1.slug',
        'promo_desc',
        'benefit',
        'basic_price',
        'promo_price',
        't1.cashback_percentage',
        't1.max_cashback',
        't1.start_date',
        't1.end_date',
        'tnc',
        'expired',
        'images',
        'quantity',
        'soldout',
        'hotdeals',
    )
        .from('merchant_promo as t1')
        .innerJoin('merchant as t2', 't2.id', 't1.fid_merchant')
        .innerJoin('merchant_category as t3','t2.fid_category', 't3.id')
        .andWhere('t1.activated',1)
        .andWhere('quantity', '>', 0)
        .andWhere('t1.fid_account_type', 1)
        // .andWhere('t2.city', 'Bandung')
        .andWhere('t1.slug', slug)
        .andWhere('end_date', '>', date.localDate())
        // .orderBy('t1.id', sort)
        // .paginate(limit, page, true)
        .then(data => {
            // console.log(paginator)
            if (data.length !== 0){
                // console.log(data)
                let bigImage = []
                let thumbImage = []
                let tncSpl = splitImage.splitTnc(data[0].benefit, data[0].tnc)
                data[0].benefit = tncSpl.objBenefit
                data[0].tnc = tncSpl.objTnc
                // console.log(tncSpl)
                data.forEach((element1, index1) => {
                    element1.images.split(',').forEach((element2, index2) => {
                        if (element2.startsWith('big')){
                            // thumbImage['img']
                            bigImage.push(element2)
                        }else{
                            thumbImage.push(element2)
                        }
                        // console.log(index+'. '+element)
                        // img['img'+index] = element
                    })
                    data[index1].images = {thumbImage: thumbImage, bigImage: bigImage}
                    thumbImage = []
                    bigImage = []
                })
                res.json({
                    success: true,
                    message: 'Sukses ambil data',
                    // count: paginator.total,
                    // current_page: paginator.current_page,
                    // last_page: paginator.last_page,
                    // limit: paginator.per_page,
                    // // sortBy: sortBy,
                    data: data,
                    // data2: tncSpl,
                });
            }else{
                res.json({
                    // name: data
                    success: true,
                    message: "Data Kosong",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }
        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'something error check your server logs.',
                data: err.toLocaleString()
            });
            console.log(err)
        });

});

// GET LIST BY MERCHANT ID
router.get('/merchant-id=:merchant_id/page=:page/limit=:limit/sort=:sort', (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    let merchant_id = req.params.merchant_id

    db.select('*')
        .from('merchant_promo')
        .where('activated', 1)
        .andWhere('fid_merchant', merchant_id)
        .orderBy('id', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            // paginator.data[0].coordinat2 = JSON.parse(paginator.data[0].coordinat2)
            console.log(paginator)
            res.json({
                success: true,
                message: "sukses ambil data promo by id merchant",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: sortBy,
                data: paginator.data,
            });
        });


});

// SEARCH REGEXP
router.get('/search1/value=:value/page=:page/limit=:limit/sort=:sort',  (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    let value = req.params.value.trimEnd().replace(/ /g, '|')

    console.log(value) // embryo

    db.select(
        't1.id',
        't1.fid_merchant',
        't2.default_image as merchant_image',
        't3.title as category',
        't3.slug as category_slug',
        't2.title as merchant_title',
        't2.address as merchant_address',
        't2.location as merchant_location',
        't2.city as merchant_city',
        'promo_title',
        't1.slug',
        'promo_desc',
        'benefit',
        'basic_price',
        'promo_price',
        't1.cashback_percentage',
        't1.max_cashback',
        't1.start_date',
        't1.end_date',
        'tnc',
        'expired',
        'images',
        'quantity',
        'soldout',
        'hotdeals',
        't1.activated'
    )
        .from('merchant_promo as t1')
        .innerJoin('merchant as t2', 't2.id', 't1.fid_merchant')
        .innerJoin('merchant_category as t3','t2.fid_category', 't3.id')
        // .whereRaw(`LOWER(promo_title) LIKE LOWER(?)`, [`%${value}%`])
        .whereRaw(`LOWER(promo_title) REGEXP LOWER(?)`, [`(${value})+.*`])
        .andWhere('t1.activated',1)
        .andWhere('quantity', '>', 0)
        .andWhere('end_date', '>', date.localDate())
        .orWhereRaw(`LOWER(t2.title) REGEXP LOWER(?)`, [`(${value})+.*`])
        .andWhere('t1.activated',1)
        .andWhere('quantity', '>', 0)
        .andWhere('end_date', '>', date.localDate())
        // .andWhere('t1.fid_account_type', 1)
        // .andWhere('t2.city', 'Bandung')
        .orderBy('t1.id', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length > 0){
                // console.log(data)
                let bigImage = []
                let thumbImage = []
                // let tncSpl = splitImage.splitTnc(paginator.data[0].benefit, paginator.data[0].tnc)
                // paginator.data[0].benefit = tncSpl.objBenefit
                // paginator.data[0].tnc = tncSpl.objTnc
                // console.log(tncSpl)
                paginator.data.forEach((element1, index1) => {
                    element1.images.split(',').forEach((element2, index2) => {
                        if (element2.startsWith('big')){
                            // thumbImage['img']
                            bigImage.push(element2)
                        }else{
                            thumbImage.push(element2)
                        }
                        // console.log(index+'. '+element)
                        // img['img'+index] = element
                    })
                    paginator.data[index1].images = {thumbImage: thumbImage, bigImage: bigImage}
                    thumbImage = []
                    bigImage = []
                })
                res.json({
                    success: true,
                    message: 'Sukses ambil data list product',
                    count: paginator.total,
                    current_page: paginator.current_page,
                    last_page: paginator.last_page,
                    limit: paginator.per_page,
                    // sortBy: sortBy,
                    data: paginator.data,
                    // data2: tncSpl,
                });
            }else{
                res.json({
                    // name: data
                    success: true,
                    message: "Data Kosong",
                    // count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: 0,
                });
            }
        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'something error check your server logs.',
                data: err.toLocaleString()
            });
            console.log(err)
        });
});

// SEARCH REGEXP 2
router.get('/search/value=:value/page=:page/limit=:limit/sort=:sort',  (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    let value = req.params.value

    // let value = req.params.value.replace(/ /g, '|')

    // console.log(date.localDate()) // embryo
    // console.log(date.localDate()) // embryo

    db.select(
        't1.id',
        't1.fid_merchant',
        't2.default_image as merchant_image',
        't3.title as category',
        't3.slug as category_slug',
        't2.title as merchant_title',
        't2.address as merchant_address',
        't2.location as merchant_location',
        't2.city as merchant_city',
        'promo_title',
        't1.slug',
        'promo_desc',
        'benefit',
        'basic_price',
        'promo_price',
        't1.cashback_percentage',
        't1.max_cashback',
        't1.start_date',
        't1.end_date',
        'tnc',
        'expired',
        'images',
        'quantity',
        'soldout',
        'hotdeals',
        't1.activated'
    )
        .from('merchant_promo as t1')
        .innerJoin('merchant as t2', 't2.id', 't1.fid_merchant')
        .innerJoin('merchant_category as t3','t2.fid_category', 't3.id')
        // .whereRaw(`CONCAT_WS('', LOWER(promo_title), LOWER(t2.title)) LIKE LOWER(?)`, [`%${value}%`])
        .whereRaw(`CONCAT_WS('', LOWER(promo_title), LOWER(t2.title)) LIKE LOWER(?)`, [`%${value}%`])
        .andWhere('t1.activated',1)
        .andWhere('quantity', '>', 0)
        .andWhere('end_date', '>', date.localDate())
        // .andWhere('t1.fid_account_type', 1)
        // .andWhere('t2.city', 'Bandung')
        .orderBy('t1.id', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length > 0){
                // console.log(data)
                let bigImage = []
                let thumbImage = []
                // let tncSpl = splitImage.splitTnc(paginator.data[0].benefit, paginator.data[0].tnc)
                // paginator.data[0].benefit = tncSpl.objBenefit
                // paginator.data[0].tnc = tncSpl.objTnc
                // console.log(tncSpl)
                paginator.data.forEach((element1, index1) => {
                    element1.images.split(',').forEach((element2, index2) => {
                        if (element2.startsWith('big')){
                            // thumbImage['img']
                            bigImage.push(element2)
                        }else{
                            thumbImage.push(element2)
                        }
                        // console.log(index+'. '+element)
                        // img['img'+index] = element
                    })
                    paginator.data[index1].images = {thumbImage: thumbImage, bigImage: bigImage}
                    thumbImage = []
                    bigImage = []
                })
                res.json({
                    success: true,
                    message: 'Sukses ambil data list product dari merchant '+paginator.data[0].merchant_title,
                    count: paginator.total,
                    current_page: paginator.current_page,
                    last_page: paginator.last_page,
                    limit: paginator.per_page,
                    // sortBy: sortBy,
                    data: paginator.data,
                    // data2: tncSpl,
                });
            }else{
                res.json({
                    // name: data
                    success: true,
                    message: "Data Kosong",
                    // count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: 0,
                });
            }
        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'something error check your server logs.',
                data: err.toLocaleString()
            });
            console.log(err)
        });
});

//FILTER
router.post('/filter/page=:page/limit=:limit',  (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    // let sortBy = req.params.sort;
    let isPopularPromo = req.body.popular_promo
    let isLowerPrice = req.body.lower_price
    let isNewestPromo = req.body.newest_promo
    let isHighestRating = req.body.highest_rating
    let min_price = req.body.min_price
    let max_price = req.body.max_price
    // console.log(req.body)

    let q = db.select(
        't1.id',
        't1.fid_merchant',
        't2.default_image as merchant_image',
        't3.title as category',
        't3.slug as category_slug',
        't2.title as merchant_title',
        't2.address as merchant_address',
        't2.location as merchant_location',
        't2.city as merchant_city',
        'promo_title',
        't1.slug',
        'promo_desc',
        'benefit',
        'basic_price',
        'promo_price',
        't1.cashback_percentage',
        't1.max_cashback',
        't1.start_date',
        't1.end_date',
        'tnc',
        'expired',
        'images',
        'quantity',
        'soldout',
        'hotdeals',
    )
        .from('merchant_promo as t1')
        .innerJoin('merchant as t2', 't2.id', 't1.fid_merchant')
        .innerJoin('merchant_category as t3','t2.fid_category', 't3.id')
        .andWhere('t1.activated',1)
        .andWhere('quantity', '>', 0)
        .andWhere('end_date', '>', date.localDate())
        .andWhereBetween('t1.promo_price', [min_price, max_price]); // min max price
        if(isPopularPromo === "true"){
            q.orderBy('t1.soldout', 'desc');
        }
        if(isLowerPrice === "true"){
            q.orderBy('t1.promo_price', 'asc');
        }
        if(isHighestRating === "true"){

        }
        if(isNewestPromo === "true"){
            q.orderBy('t1.created', 'desc');
        }
        q.paginate(limit, page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length > 0){
                // console.log(data)
                let bigImage = []
                let thumbImage = []
                // let tncSpl = splitImage.splitTnc(paginator.data[0].benefit, paginator.data[0].tnc)
                // paginator.data[0].benefit = tncSpl.objBenefit
                // paginator.data[0].tnc = tncSpl.objTnc
                // console.log(tncSpl)
                paginator.data.forEach((element1, index1) => {
                    element1.images.split(',').forEach((element2, index2) => {
                        if (element2.startsWith('big')){
                            // thumbImage['img']
                            bigImage.push(element2)
                        }else{
                            thumbImage.push(element2)
                        }
                        // console.log(index+'. '+element)
                        // img['img'+index] = element
                    })
                    paginator.data[index1].images = {thumbImage: thumbImage, bigImage: bigImage}
                    thumbImage = []
                    bigImage = []
                })
                res.json({
                    success: true,
                    message: 'Sukses ambil data nearby product',
                    count: paginator.total,
                    current_page: paginator.current_page,
                    last_page: paginator.last_page,
                    limit: paginator.per_page,
                    // sortBy: sortBy,
                    data: paginator.data,
                    // data2: tncSpl,
                });
            }else{
                res.json({
                    // name: data
                    success: true,
                    message: "Data Kosong",
                    // count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: 0,
                });
            }
        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'something error check your server logs.',
                data: err.toLocaleString()
            });
            console.log(err)
        });
});

// NEARBY
router.post('/nearby/page=:page/limit=:limit/sort=:sort', (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    let city = req.body.city
    let location = req.body.location
    let cat_id = req.body.cat_id
    // console.log(req.body)
    let q = db.select(
        't1.id',
        't1.fid_merchant',
        't2.title as merchant_title',
        't2.address as merchant_address',
        't2.location as merchant_location',
        't2.city as merchant_city',
        't2.default_image as merchant_image',
        'promo_title',
        'promo_desc',
        'benefit',
        'basic_price',
        'promo_price',
        't1.cashback_percentage',
        't1.max_cashback',
        't1.start_date',
        't1.end_date',
        'tnc',
        'expired',
        'images',
        'quantity',
        'soldout',
        'hotdeals',
        't3.slug as slug_promo_type',
        't3.name as promo_type_name',
        't3.desc as promo_type_desc',
        't1.slug as promo_slug',
        't4.title as merchant_category',
        't4.slug as category_slug',
        't4.color as merchant_color',
    )
        q.from('merchant_promo as t1')
        .innerJoin('merchant as t2', 't2.id', 't1.fid_merchant')
        .innerJoin('mst_promo_type as t3','t1.promo_type_id', 't3.id')
        .innerJoin('merchant_category as t4','t2.fid_category', 't4.id')
        if (cat_id !== "0" && city !== "0" && location !== "0"){
            q.whereRaw('(LOWER(t2.city) = LOWER(?) AND t2.fid_category = ?) OR (LOWER(t2.location) = LOWER(?) AND t2.fid_category = ?)', [city, cat_id, location, cat_id])
        }else{
            q.whereRaw('LOWER(t2.city) = LOWER(?) OR LOWER(t2.location) = LOWER(?)', [city, location])
        }
        q.andWhere('t1.activated', 1)
        .andWhere('quantity', '>', 0)
            // .andWhere('city', 'bali')
        .andWhere('t1.end_date', '>', date.localDate())
        // .andWhere('t1.end_date', '>', date.localDate())
        // .andWhere('t1.promo_type_id', type)

    q.orderBy('id', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            let bigImage = []
            let thumbImage = []
            paginator.data.forEach((element1, index1) => {
                element1.images.split(',').forEach((element2, index2) => {
                    if (element2.startsWith('big')){
                        // thumbImage['img']
                        bigImage.push(element2)
                    }else{
                        thumbImage.push(element2)
                    }
                    // console.log(index+'. '+element)
                    // img['img'+index] = element
                })
                paginator.data[index1].images = {thumbImage: thumbImage, bigImage: bigImage}
                thumbImage = []
                bigImage = []
            })

            // console.log(paginator.total)
            if (paginator.data.length !== 0){
                res.json({
                    success: true,
                    message: 'Sukses ambil data '+paginator.data[0].slug_promo_type,
                    count: paginator.total,
                    current_page: paginator.current_page,
                    limit: limit,
                    // sortBy: sortBy,
                    // type_name: paginator.data[0].promo_type_name,
                    data: paginator.data
                });
            }else{
                res.json({
                    // name: data
                    success: true,
                    message: "Data Kosong",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }
        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'something error check your server logs.',
                data: err.toLocaleString()
            });
            console.log(err)
        });


});

module.exports = router;