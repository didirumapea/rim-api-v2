const express = require('express');
const router = express.Router();
const db = require('../../../database').db_rim; // as const knex = require('knex')(config);
const moment = require('moment');
const setupPaginator = require('knex-paginator');
const checkAuth = require('../../../middleware/check-auth')
const jwt = require('jsonwebtoken')
const randomstring = require('randomstring')
const date = require('../../../config/moment-date-format')

// bcrypt config
// const bcrypt = require('bcrypt');
// const saltRounds = 10;
// const myPlaintextPassword = 's0/\/\P4$$w0rD';
// const someOtherPlaintextPassword = 'not_bacon';
setupPaginator(db);


router.get('/', (req, res) => {
    res.send('We Are In HOMEPAGE Route')
})

router.get('/product/type=:type/page=:page/limit=:limit', (req, res) => {

    // legend
    // type = 1 => All Product
    // type = 2 => Featured
    // type = 3 => Promo Terpopuler
    // type = 4 => Hotel Murah
    // type = 5 => Rekomendasi Kuliner Terbaik!

    let type = req.params.type;
    let page = req.params.page
    let limit = req.params.limit
    // console.log(date.localDate())
    db.select(
        't1.id',
        't1.fid_merchant',
        't2.title as merchant_title',
        't2.address as merchant_address',
        't2.location as merchant_location',
        't2.city as merchant_city',
        't2.default_image as merchant_image',
        'promo_title',
        'promo_desc',
        'benefit',
        'basic_price',
        'promo_price',
        't1.cashback_percentage',
        't1.max_cashback',
        't1.start_date',
        't1.end_date',
        'tnc',
        'expired',
        'images',
        'quantity',
        'soldout',
        'hotdeals',
        't3.slug as slug_promo_type',
        't3.name as promo_type_name',
        't3.desc as promo_type_desc',
        't1.slug as promo_slug',
        't4.title as merchant_category',
        't4.slug as category_slug',
        't4.color as merchant_color',
    )
        .from('merchant_promo as t1')
        .innerJoin('merchant as t2', 't2.id', 't1.fid_merchant')
        .innerJoin('mst_promo_type as t3','t1.promo_type_id', 't3.id')
        .innerJoin('merchant_category as t4','t2.fid_category', 't4.id')
        .andWhere('t1.activated', 1)
        .andWhere('t3.activated', 1)
        .andWhere('quantity', '>', 0)
        .andWhere('t1.end_date', '>', date.localDate())
        .andWhere('t3.end_date', '>', date.localDate())
        .andWhere('t1.promo_type_id', type)
        .paginate(limit, page, true)
        .then(paginator => {
            let bigImage = []
            let thumbImage = []
            paginator.data.forEach((element1, index1) => {
                element1.images.split(',').forEach((element2, index2) => {
                    if (element2.startsWith('big')){
                        // thumbImage['img']
                        bigImage.push(element2)
                    }else{
                        thumbImage.push(element2)
                    }
                    // console.log(index+'. '+element)
                    // img['img'+index] = element
                })
                paginator.data[index1].images = {thumbImage: thumbImage, bigImage: bigImage}
                thumbImage = []
                bigImage = []
            })

            // console.log(paginator.total)
            if (paginator.data.length !== 0){
                res.json({
                    success: true,
                    message: 'Sukses ambil data '+paginator.data[0].slug_promo_type,
                    count: paginator.total,
                    current_page: paginator.current_page,
                    limit: limit,
                    // sortBy: sortBy,
                    type_name: paginator.data[0].promo_type_name,
                    data: paginator.data
                });
            }else{
                res.json({
                    // name: data
                    success: true,
                    message: "Data Kosong",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }
        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'something error check your server logs.',
                data: err.toLocaleString()
            });
            console.log(err)
        });

});
// VIEW TYPE CODE
router.get('/promo-view-code/type=:type/page=:page/limit=:limit',  (req, res) => {

    // legend
    // type = 1 => Layer 1
    // type = 2 => Layer 2
    // type = 3 => Layer 3
    // type = 4 => Layer 4

    let type = req.params.type;
    let page = req.params.page
    let limit = req.params.limit
    // console.log(date.localDate())
    db.select(
        '*'
    )
        .from('mst_promo_type')
        .where('view_type_code', type)
        .andWhere('activated', 1)
        .then( async (data) => {
            if (data.length > 0) {
                // console.log(data)
                data.forEach(async (element, index) => {
                    // console.log(data)
                    const q = await db.select(
                        't1.id',
                        't1.fid_merchant',
                        't2.title as merchant_title',
                        't2.address as merchant_address',
                        't2.location as merchant_location',
                        't2.city as merchant_city',
                        't2.default_image as merchant_image',
                        'promo_title',
                        'promo_desc',
                        'benefit',
                        'basic_price',
                        'promo_price',
                        't1.cashback_percentage',
                        't1.max_cashback',
                        't1.start_date',
                        't1.end_date',
                        'tnc',
                        'expired',
                        'images',
                        'quantity',
                        'soldout',
                        'hotdeals',
                        't3.slug as slug_promo_type',
                        't3.name as promo_type_name',
                        't3.desc as promo_type_desc',
                        't1.slug as promo_slug',
                        't4.title as merchant_category',
                        't4.slug as category_slug',
                        't4.color as merchant_color',
                    )
                        .from('merchant_promo as t1')
                        .innerJoin('merchant as t2', 't2.id', 't1.fid_merchant')
                        .innerJoin('mst_promo_type as t3', 't1.promo_type_id', 't3.id')
                        .innerJoin('merchant_category as t4', 't2.fid_category', 't4.id')
                        .andWhere('t1.activated', '=', 1)
                        .andWhere('quantity', '>', 0)
                        .andWhere('t1.end_date', '>', date.localDate())
                        .andWhere('t1.promo_type_id', element.id)
                        .paginate(limit, page, true)
                        .then(paginator => {
                            let bigImage = []
                            let thumbImage = []
                            paginator.data.forEach((element1, index1) => {
                                element1.images.split(',').forEach((element2, index2) => {
                                    if (element2.startsWith('big')) {
                                        // thumbImage['img']
                                        bigImage.push(element2)
                                    } else {
                                        thumbImage.push(element2)
                                    }
                                    // console.log(index+'. '+element)
                                    // img['img'+index] = element
                                })
                                paginator.data[index1].images = {thumbImage: thumbImage, bigImage: bigImage}
                                thumbImage = []
                                bigImage = []
                            })

                            // console.log(paginator.total)
                            if (paginator.data.length !== 0) {
                                // data[index].list = []
                                // data[index].list = paginator.data
                                return paginator.data
                                // res.json({
                                //     success: true,
                                //     message: 'Sukses ambil data '+paginator.data[0].slug_promo_type,
                                //     count: paginator.total,
                                //     current_page: paginator.current_page,
                                //     limit: limit,
                                //     // sortBy: sortBy,
                                //     type_name: paginator.data[0].promo_type_name,
                                //     data: paginator.data
                                // });
                            } else {
                                console.log(`PROMO PADA PROMO_TYPE ${element.id} KOSONG`)
                                // res.json({
                                //     // name: data
                                //     success: true,
                                //     message: "Data Kosong",
                                //     // count: paginator.data.length,
                                //     // current_page: paginator.current_page,
                                //     // limit: paginator.data.length,
                                //     // sortBy: sortBy,
                                //     // data: data,
                                // });
                            }
                        })
                        .catch((err) => {
                            res.json({
                                // name: data
                                success: false,
                                message: 'something error check your server logs.',
                                data: err.toLocaleString()
                            });
                            console.log(err)
                        });
                    data[index].list = q
                    // console.log(q)
                    if(index === data.length-1){
                        res.json({
                            // name: data
                            success: true,
                            message: "Sukses ambil data",
                            data: data
                        });
                    }
                })
            } else {
                res.json({
                    // name: data
                    success: true,
                    message: 'Get layer success, but empty',
                    data: data
                });
            }
            // res.json({
            //     // name: data
            //     success: true,
            //     message: 'Get layer success',
            //     data: data
            // });
        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'home.js : something error check your server logs.',
                data: err.toLocaleString()
            });
        })
});

router.get('/banner=:pos',  (req, res) => {
    let pos = req.params.pos;
    db.select(
        'position',
        'title',
        'desc',
        'image'
        )
        .from('banner')
        .where('activated', 1)
        .andWhere('position', pos)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    count: data.length,
                    data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil banner",
                    count: data.length,
                    data: data,
                });
            }
        });

});

router.get('/category',  (req, res) => {
    db.select(
        'id',
        'title',
        'slug',
        'image',
        'color'
    )
        .from('merchant_category')
        .where('activated', '=', 1)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil category",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }
        });

});

router.get('/list-merchant/page=:page/limit=:limit',  (req, res) => {
    let page = req.params.page
    let limit = req.params.limit
    // let sort = req.params.sort

    db.select(
        't1.id',
        't1.title as merchant_name',
        't2.title as category_merchant',
        // 'desc',
        // 't1.slug',
        'default_image'

    )
        .from('merchant as t1')
        .innerJoin('merchant_category as t2', 't1.fid_category', 't2.id')
        .where('t1.activated',2)
        .paginate(limit, page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    // count: paginator.total,
                    // current_page: paginator.current_page,
                    // limit: limit,
                    // data: paginator.data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil list merchant",
                    count: paginator.total,
                    current_page: paginator.current_page,
                    limit: limit,
                    data: paginator.data,
                });
            }
        })
        .catch((err) => {
            console.log(err)
        });

});

module.exports = router;