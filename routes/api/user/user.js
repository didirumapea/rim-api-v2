const express = require('express');
const router = express.Router();
const db = require('../../../database').db_rim; // as const knex = require('knex')(config);
const moment = require('moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const date = require('../../../config/moment-date-format')
let FCM = require('fcm-node');
let serverKey = 'AAAAt9cMdUg:APA91bGArRIER_RTKSda60ylHg-CGzEthGJcacJex7Ejgp_1axOw4hxyhafdSxPt4_vXNXC5AO_oabcd1IsfcGGaNTGecRKKO9BBGCDk1jpW5fxwnUddueh5d335x8Ese3Jp361QPrR1'; //put your server key here
let fcm = new FCM(serverKey);
const checkAuth = require('../../../middleware/check-auth')

let dateNow = moment().format("YYYY-MM-DD hh:mm:ss")

// GET TRANSAKSI
router.get('/my-transaksi/page=:page/limit=:limit/sort=:sort', (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    let id_user = req.userData.id
    // let remember_token = req.body.remember_token
    db.select(
        't2.promo_title',
        't1.created as tgl_transaksi',
        't1.transaction_status as status',
        't1.total',

    )
        .from('ecommerce_transaction as t1')
        .innerJoin('merchant_promo as t2', 't1.fid_promo', 't2.id')
        .where('t1.fid_user', id_user)
        .orderBy('t1.created', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            if (paginator.data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    current_page: paginator.current_page,
                    limit: paginator.data.length,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    sortBy: sortBy,
                    data: paginator.data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil data transaksi",
                    current_page: paginator.current_page,
                    limit: paginator.data.length,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    sortBy: sortBy,
                    data: paginator.data,
                });
            }
        });

});

// GET TRANSAKSI CASHBACK
router.get('/my-transaksi-cashback/page=:page/limit=:limit/sort=:sort', (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    let id_user = req.userData.id
    // let remember_token = req.body.remember_token
    db.select(
        't2.promo_title',
        't1.created as tgl_transaksi',
        't1.transaction_status as status',
        't1.cashback',
    )
        .from('ecommerce_transaction as t1')
        .innerJoin('merchant_promo as t2', 't1.fid_promo', 't2.id')
        .where('t1.fid_user', id_user)
        .orderBy('t1.created', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            if (paginator.data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    current_page: paginator.current_page,
                    limit: paginator.data.length,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    sortBy: sortBy,
                    data: paginator.data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil data transaksi",
                    current_page: paginator.current_page,
                    limit: paginator.data.length,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    sortBy: sortBy,
                    data: paginator.data,
                });
            }
        });
    // res.json({
    //     message: 'Post Created',
    //     authData
    // })

});

// GET TRANSAKSI
router.get('/mycashback/page=:page/limit=:limit/sort=:sort', (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    let id_user = req.userData.id
    // let remember_token = req.body.remember_token
    db.select(
        'point_cash_back'

    )
        .from('members')
        // .innerJoin('merchant_promo as t2', 't1.fid_promo', 't2.id')
        .where('id', id_user)
        // .orderBy('t1.created', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            if (paginator.data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    current_page: paginator.current_page,
                    limit: paginator.data.length,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    sortBy: sortBy,
                    data: paginator.data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil data transaksi",
                    current_page: paginator.current_page,
                    limit: paginator.data.length,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    sortBy: sortBy,
                    data: paginator.data,
                });
            }
        });
    // res.json({
    //     message: 'Post Created',
    //     authData
    // })

});

// GET DETAILS
router.get('/details', (req, res) => {
    console.log(req.userData)
    // let id_user = req.userData.id
    // let remember_token = req.body.remember_token
    res.json({
        success: true,
        message: "Sukses beli",
        // current_page: paginator.current_page,
        // limit: paginator.data.length,
        // sortBy: sortBy,
        data: req.userData,
    });

});

// MY CASHBACK
router.get('/mycashback2', (req, res) => {
    // let page = req.params.page;
    let id_user = req.userData.id
    // let remember_token = req.body.remember_token
    db.select(
    'point_cash_back'
    )
        .from('members')
        .where('id', id_user)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil cashback user",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }
        });
    // res.json({
    //     message: 'Post Created',
    //     authData
    // })

});

// GET MY VOUCHER
router.get('/myvoucher/trans-status=:trans_status/operator=:operator/is-used=:is_used/page=:page/limit=:limit/sort=:sort/tz-offset=:tz', (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    let trans_status = req.params.trans_status;
    let operator = req.params.operator;
    let is_used = req.params.is_used;
    let tzOffset = req.params.tz;
    // console.log(date.getUtcTimezoneWithOffset(parseInt(tzOffset)))
    // console.log(moment().add('1', 'hours').format("YYYY-MM-DD HH:mm:ss"))

    let q = db.select(
        't1.id',
        't2.images',
        't2.promo_title as promo_title',
        't2.promo_price as price',
        't2.cashback_percentage',
        't2.tnc',
        't2.benefit',
        't2.hotdeals',
        't2.slug as promo_slug',
        't3.title as merchant_title',
        't3.address as merchant_address',
        't3.location as merchant_location',
        't4.title as merchant_category',
        //
        't1.fid_user',
        't1.transaction_id',
        'inv_code',
        'voucher_code',
        'voucher_promo_code',
        'rtc_unique_code',
        'transaction_status',
        'payment_type',
        'redirect_url',
        't1.quantity',
        't1.basic_price',
        'tax',
        'total',
        'cashback',
        'paid_date',
        'expired_voucher',
        't1.created as order_date',
        'used_date'
    )
        .from('ecommerce_transaction as t1')
        .innerJoin('merchant_promo as t2', 't1.fid_promo', 't2.id')
        .innerJoin('merchant as t3', 't2.fid_merchant', 't3.id')
        .innerJoin('merchant_category as t4', 't3.fid_category', 't4.id')
        if (trans_status === 'pending'){
            q.whereRaw(`? BETWEEN t1.created AND DATE_ADD(t1.created, INTERVAL ${parseInt(tzOffset)+1} HOUR) and transaction_status in (?, ?)`, [date.getUtcTimezoneWithOffset(parseInt(tzOffset)), trans_status, 'unverification'])
        }else{
            q.andWhere('transaction_status', operator, trans_status)
        }
        q.andWhere('used', is_used)
        .andWhere('t1.fid_user', req.userData.id)
        .orderBy('id', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            if (paginator.data.length > 0){
                let bigImage = []
                let thumbImage = []
                paginator.data.forEach((element1, index1) => {
                    paginator.data[index1].expired_voucher !== 'Invalid date' ? paginator.data[index1].expired_voucher = moment(element1.expired_voucher).format('YYYY-MM-DD') : ''
                    element1.used_date !== 'Invalid date' ?  paginator.data[index1].used_date = moment(element1.used_date).format('YYYY-MM-DD') : ''
                    paginator.data[index1].order_date = date.getUtcTimezoneWithOffset(parseInt(tzOffset), element1.order_date)
                    // console.log(moment(element1.expired_voucher).format('YYYY-MM-DD'))
                    element1.images.split(',').forEach((element2, index2) => {
                        if (element2.startsWith('big')){
                            // thumbImage['img']
                            bigImage.push(element2)
                        }else{
                            thumbImage.push(element2)
                        }
                        // console.log(index+'. '+element)
                        // img['img'+index] = element
                    })
                    paginator.data[index1].images = {thumbImage: thumbImage, bigImage: bigImage}
                    thumbImage = []
                    bigImage = []
                })
                res.json({
                    success: true,
                    message: "sukses ambil my voucher",
                    current_page: paginator.current_page,
                    limit: paginator.data.length,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    sortBy: sortBy,
                    data: paginator.data,
                });
            }else{
                res.json({
                    // name: data
                    success: true,
                    message: "Data Kosong",
                    // count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: 0,
                });
            }

        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'something error check your server logs.',
                data: err.toLocaleString()
            });
            console.log(err)
        });
});

// USE MY VOUCHER

router.post('/myvoucher/used-voucher/id=:id', (req, res) =>  {
    let details = {
        used: 1,
        used_date: date.utc()
    }

    db.select(
        '*'
    )
        .from('ecommerce_transaction')
        .where('id', req.params.id)
        .andWhere('fid_user', req.userData.id)
        .andWhere('transaction_status', 'settlement')
        .then((data) => {
            if(data.length > 0){
                // console.log(data[0].used)
                if (data[0].used === 1){
                    res.json({
                        success: false,
                        message: "Voucher sudah digunakan",
                        // count: data.length,
                        // data: data,
                    });
                }else{
                    db('ecommerce_transaction')
                        .where('id', req.params.id)
                        .andWhere('fid_user', req.userData.id)
                        .update(details)
                        .then(data2 => {
                            res.json({
                                success: true,
                                message: "Voucher berhasil digunakan",
                                // count: data.length,
                                data: data,
                                data2: data2,
                            });

                        })
                }
            }else{
                res.json({
                    success: false,
                    message: `Data voucher ${req.params.id} tidak di temukan`,
                    // count: data.length,
                    data: data,
                });
            }

        })
        .catch((err) =>{
        console.log(err)
        res.json({
            success: false,
            message: "Update admin failed.",
            // count: data.length,
            data: err,
        });
    });
});

// ADD Wishlist
router.post('/add/wishlist' , (req, res) => {
    // let account_type = req.body.account_type
    let details = {
        fid_user: req.userData.id,
        fid_promo: req.body.fid_promo,
    }

    db.select(
        'id'
    )
        .from('wishlist')
        .where('fid_promo', details.fid_promo)
        .then((data) => {
            if (data.length === 0){
                db('wishlist')
                    .insert(details)
                    .then(data => {
                        res.json({
                            success: true,
                            message: "Tambah wishlist sukses",
                            data: data
                            // token: token
                        });
                    })
                    .catch((err) => {
                        res.json({
                            success: false,
                            message: "Tambah wishlist gagal",
                            data: err
                            // token: token
                        });
                        console.log(err)
                    });
            }else{
                res.json({
                    success: false,
                    message: "Promo ini sudah terdaftar di wishlist",
                    data: data
                    // token: token
                });
            }
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "failed add wishlist",
                data: err
                // token: token
            });
        })
});
// Get Wishlist
router.get('/wishlist/page=:page/limit=:limit/sort=:sort', (req, res) =>    {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    let id_user = req.userData.id
    // let remember_token = req.body.remember_token
    db.select(
        't1.id',
        't2.fid_merchant',
        't3.default_image as merchant_image',
        't4.title as category',
        't4.slug as category_slug',
        't3.title as merchant_name',
        't3.address as merchant_address',
        't3.location as merchant_location',
        't3.city as merchant_city',
        't2.promo_title',
        't2.slug',
        't2.promo_desc',
        't2.benefit',
        't2.basic_price',
        't2.promo_price',
        't2.cashback_percentage',
        't2.max_cashback',
        't2.start_date',
        't2.end_date',
        't2.tnc',
        't2.expired',
        't2.images',
        't2.quantity',
        't2.soldout',
    )
        .from('wishlist as t1')
        .innerJoin('merchant_promo as t2', 't1.fid_promo', 't2.id')
        .innerJoin('merchant as t3', 't2.fid_merchant', 't3.id')
        .innerJoin('merchant_category as t4', 't3.fid_category', 't4.id')
        .where('t1.activated', 1)
        .where('t1.fid_user', id_user)
        // .orderBy('t1.created', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            if (paginator.data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    current_page: paginator.current_page,
                    limit: paginator.data.length,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    sortBy: sortBy,
                    data: paginator.data,
                });
            }else{
                let bigImage = []
                let thumbImage = []
                paginator.data.forEach((element1, index1) => {
                    element1.images.split(',').forEach((element2, index2) => {
                        if (element2.startsWith('big')){
                            // thumbImage['img']
                            bigImage.push(element2)
                        }else{
                            thumbImage.push(element2)
                        }
                        // console.log(index+'. '+element)
                        // img['img'+index] = element
                    })
                    paginator.data[index1].images = {thumbImage: thumbImage, bigImage: bigImage}
                    thumbImage = []
                    bigImage = []
                })
                res.json({
                    success: true,
                    message: "Sukses ambil data wishlist",
                    current_page: paginator.current_page,
                    limit: paginator.data.length,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    sortBy: sortBy,
                    data: paginator.data,
                });
            }
        });
    // res.json({
    //     message: 'Post Created',
    //     authData
    // })

});
// Get Notifications
router.get('/notifications/page=:page/limit=:limit/sort=:sort', (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    let id_user = req.userData.id
    // let remember_token = req.body.remember_token
    db.select(
        't1.id',
        't1.notif_type',
        't2.code_title_notif',
        't1.created',
    )
        .from('notifications as t1')
        .innerJoin('mst_code as t2', 't1.mst_code_id', 't2.id')
        .where('t1.activated', 1)
        .where('t1.fid_user', id_user)
        // .orderBy('t1.created', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            if (paginator.data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    current_page: paginator.current_page,
                    limit: paginator.data.length,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    sortBy: sortBy,
                    data: paginator.data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil data wishlist",
                    current_page: paginator.current_page,
                    limit: paginator.data.length,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    sortBy: sortBy,
                    data: paginator.data,
                });
            }
        });
    // res.json({
    //     message: 'Post Created',
    //     authData
    // })

});


// TESTING
router.get('/test/firebase-messaging', (req, res) => {


    let message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
        to: 'BA-tLqbyRkBtzBSQ5OlmJiGENorRJ5EUA-r6oqhf00lXjEtlDUlsfcWof6nUB7fUEAnVA4jvmYndww4NlyOWPUM',
        collapse_key: 'your_collapse_key',

        notification: {
            title: 'Title of your push notification',
            body: 'Body of your push notification'
        },

        data: {  //you can send only notification or only data(or include both)
            my_key: 'my value',
            my_another_key: 'my another value'
        }
    };

    fcm.send(message, function(err, response){
        if (err) {
            console.log("Something has gone wrong!");
        } else {
            console.log("Successfully sent with response: ", response);
        }
    });


    res.json({
        message: 'Post Created',
    })

});

router.get('/test/sitemap', (req, res) => {
    const write = require('write');
    // write.sync('test.xml', 'some many folks data in the new world...', { overwrite: true });
    // let createFile = require('create-file');

    // createFile('test-data.xml', 'my contentersss\n', function (err) {
    //     // file either already exists or is now created (including non existing directories)
    // });

    const { SitemapStream, streamToPromise } = require('sitemap')
// Creates a sitemap object given the input configuration with URLs
    const sitemap = new SitemapStream({ hostname: 'http://example.com' });
    sitemap.write({ url: '/page-1/', changefreq: 'daily', priority: 0.3, lastmod: '2019-10-21T04:23:32+00:00'})
    sitemap.write({ url: '/page-2/', changefreq: 'daily', priority: 0.3, lastmod: '2019-10-21T04:23:32+00:00' })
    sitemap.write({ url: '/page-3/', changefreq: 'daily', priority: 0.3, lastmod: '2019-10-21T04:23:32+00:00' })
    sitemap.write({ url: '/page-4/', changefreq: 'daily', priority: 0.3, lastmod: '2019-10-21T04:23:32+00:00' })
    sitemap.write({ url: '/page-5/', changefreq: 'daily', priority: 0.3, lastmod: '2019-10-21T04:23:32+00:00' })
    sitemap.write({ url: '/page-6/', changefreq: 'daily', priority: 0.3, lastmod: '2019-10-21T04:23:32+00:00' })
    // sitemap.write('/page-2')
    sitemap.end()

    streamToPromise(sitemap)
        .then((sm) => {
            write.sync('sitemap.xml', sm.toString(), { overwrite: true });
            // console.log(sm.toString())
        })
        .catch(console.error);


    res.json({
        message: 'Test Sitemap Success',
    })

});

module.exports = router;
