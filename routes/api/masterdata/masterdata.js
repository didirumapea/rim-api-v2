const express = require('express');
const router = express.Router();
const db = require('../../../database').db_rim; // as const knex = require('knex')(config);
const moment = require('moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const date = require('../../../config/moment-date-format')


let dateNow = moment().format("YYYY-MM-DD hh:mm:ss")

router.get('/', (req, res) => {
    // console.log(req.query)
    res.send('We Are In MASTERDATA Route')
})

// GET LIST
router.get('/promo-type/category/page=:page/limit=:limit/sort=:sort', (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;

    db.select('slug')
        .from('mst_promo_type')
        .union(function() {
        this.select('slug')
            .from('merchant_category')
    })
    .paginate(limit, page, true)
    .then(paginator => {
        // console.log(paginator)
        res.json({
            success: true,
            message: "sukses ambil data promo type",
            current_page: paginator.current_page,
            limit: paginator.data.length,
            paginate: {
                totalRow: paginator.total,
                from: paginator.from,
                to: paginator.to,
                currentPage: paginator.current_page,
                lastPage: paginator.last_page
            },
            sortBy: sortBy,
            data: paginator.data,
        });
    });
});


module.exports = router;