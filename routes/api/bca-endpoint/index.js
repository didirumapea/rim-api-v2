const express = require('express');
const router = express.Router();
const db = require('../../../database').db_rim; // as const knex = require('knex')(config);
const moment = require('moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const jwt = require('jsonwebtoken')
const config = require('../../../config/config')
const checkAuth = require('../../../middleware/check-auth-bca-x-rimember')
const randomstring = require("randomstring");
// --------------------------------------------------------------------------------
const request = require('request');


let dateNow = moment().format("YYYY-MM-DD hh:mm:ss")

router.get('/', (req, res) => {
    // console.log(req.query)
    res.send('We Are In BCA RESPONSE ROUTE')
})

// GENERATE VA NUMBER
router.get('/generate-va-number',  (req, res) => {
    let randomBcaVaNumber = randomstring.generate({
        length: 4,
        capitalization: 'uppercase',
        charset: 'numeric'
    });
    let idPromo = '10'
    let idUser = '565'
    let idMerc = '23'
    let vaNumber = idPromo + idUser + idMerc + randomBcaVaNumber
        res.json({
        // name: data
        success: true,
        message: 'test',
        data: vaNumber
    });

});
// GENERATE TOKEN USING VA NUMBER
router.get('/generate/token/va-number=:va_number',  (req, res) => {
    let va_number = req.params.va_number
    // console.log(catId, page, limit, sort, min, max, rating)
    // console.log(req)
    // let vaNumber = companyCode + idTrans + idPromo + idUser + idMerc
    const data = {
        va_number: va_number,
    }
    jwt.sign(data, config.jwtSecretKeyBcaResponse, {expiresIn: '30d'}, (err, token) => {
        console.log(token)
        data.token = token
        // console.log(data)
        // req.session.authUser = { user }
        res.json({
            success: true,
            message: "Generate success",
            // count: data.length,
            data: data
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "Generate failed",
                // count: data.length,
                data: err
            })
        });
    })
    // res.json({
    //     success: true,
    //     message: 'test',
    //     data: 'test'
    // });

});

// GENERATE TOKEN USING VA NUMBER
router.get('/bill-details',  checkAuth, (req, res) => {
    // console.log(req.userData)
    // console.log(req)
    // let vaNumber = companyCode + idTrans + idPromo + idUser + idMerc
    db.select(
        'inv_code',
        'total',
    )
        .from('ecommerce_transaction')
        .where('va_number', req.userData.va_number)
        .then(data => {
            if (data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil data transaksi",
                    data: data,
                });
            }
        });
});

router.get('/bypass-bmkg', (req, res) => {
    // console.log(req.userData)
    // console.log(req)
    // let vaNumber = companyCode + idTrans + idPromo + idUser + idMerc
    request('http://data.bmkg.go.id/gempadirasakan.xml', function (error, response, body) {
        console.error('error:', error); // Print the error if one occurred
        console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        console.log('body:', body); // Print the HTML for the Google homepage.
        res.set('Content-Type', 'text/xml');
        res.send(body);
        // res.send({
        //     success: true,
        //     message: 'oke good',
        //     data: body
        // })
    });



});

module.exports = router;