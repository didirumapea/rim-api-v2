const express = require('express');
const router = express.Router();
const db = require('../../../database').db_rim; // as const knex = require('knex')(config);
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const moment = require('moment');
// const autocorrect = require('autocorrect')()

router.get('/', (req, res) => {
    // console.log(req.query)
    res.send('We Are In Code Route')
})

// REDEEM VOUCHER CODE
router.get('/check/fid_promo=:fid_promo/code=:code/tz=:tzone',  (req, res) => {
    let code = req.params.code
    let fid_promo = req.params.fid_promo
    // let tz = parseInt(req.params.tzone)
    // console.log(catId, page, limit, sort, min, max, rating)
    // console.log(moment.utc().format("YYYY-MM-DD HH:mm:ss"))
    db.select(
        '*'
    )
        .from('mst_code')
        .where('code_name', code)
        .andWhere('activated', 1)
        .then(data => {
            // console.log(data)
            if (data.length !== 0){
                if (data[0].used < data[0].quantity){
                    switch (data[0].code_type) {
                        case 'category':
                            // console.log(data[0].code_type_value.split(','))
                        let q = db.select(
                                't1.id'
                            )
                                .from('merchant_promo as t1')
                                .innerJoin('merchant as t2', 't1.fid_merchant', 't2.id')
                                if(data[0].code_operator === '='){
                                    q.whereIn('t2.fid_category', data[0].code_type_value.split(','))
                                }else{
                                    q.whereNotIn('t2.fid_category', data[0].code_type_value.split(','))
                                }
                                q.andWhere('t1.id', fid_promo)
                                .then((data2) => {
                                    if(data2.length > 0){
                                        res.json({
                                            success: true,
                                            message: 'Kode bisa digunakan pada category ini',
                                            data: data,
                                        });
                                    }else{
                                        res.json({
                                            success: false,
                                            message: 'Kode tidak bisa digunakan pada category ini',
                                            data: data2,
                                        });
                                    }
                                })
                            break;
                        case 'merchant':
                            db.select(
                                'id'
                            )
                                .from('merchant_promo')
                                .where('id', fid_promo)
                                .andWhere('fid_merchant', data[0].code_type_value)
                                .then((data2) => {
                                    if(data2.length > 0){
                                        res.json({
                                            success: true,
                                            message: 'Kode bisa digunakan pada merchant ini',
                                            data: data,
                                        });
                                    }else{
                                        res.json({
                                            success: false,
                                            message: 'Kode tidak bisa digunakan pada merchant ini',
                                            data: data2,
                                        });
                                    }
                                })
                            break;
                        case 'product':
                            // console.log(data[0].code_type_value.toString(), fid_promo)
                            if(data[0].code_type_value.toString() === fid_promo.toString()){
                                res.json({
                                    success: true,
                                    message: 'Kode bisa digunakan pada product ini',
                                    data: data,
                                });
                            }else{
                                res.json({
                                    success: false,
                                    message: 'Kode tidak bisa digunakan pada product ini',
                                    // data: data2,
                                });
                            }
                            break;
                        case 'all':
                            // console.log(data[0].code_type_value.toString(), fid_promo)
                            res.json({
                                success: true,
                                message: 'Kode bisa digunakan',
                                data: data,
                            });
                            break;
                        default:
                            res.json({
                                success: false,
                                message: 'ERROR, CEK UR CODE AT code.js',
                                // data: data2,
                            });
                            break;
                    }
                }else{
                    res.json({
                        success: false,
                        message: 'Code sudah mencapai limit',
                        // data: data,
                    });
                }

            }else{
                res.json({
                    // name: data
                    success: false,
                    message: "Voucher tidak di temukan",
                    count: data.length,

                });
            }
        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'something error check your server logs.',
                data: err.toLocaleString()
            });
            console.log(err)
        });

});

module.exports = router;