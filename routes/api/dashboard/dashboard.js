const express = require('express');
const router = express.Router();
const db = require('../../../database').db_rim; // as const knex = require('knex')(config);
const moment = require('moment');
const setupPaginator = require('knex-paginator');
const date = require('../../../config/moment-date-format')
setupPaginator(db);


let dateNow = moment().format("YYYY-MM-DD hh:mm:ss")


router.get('/daily-sales', (req, res) => {
    // let page = req.params.page;
    let dateNow = moment().add('1', 'days').format('YYYY-MM-DD')
    let dateLast7Days = moment().subtract('7', 'days').format('YYYY-MM-DD')
    // console.log(moment().subtract('7', 'days').format('YYYY-MM-DD'))
    // console.log(moment().add('1', 'days').format('YYYY-MM-DD'))
    db.select(db.raw(
        'count(*) total_transaksi, sum(quantity) as total_sales, DATE_FORMAT(paid_date,\'%Y-%m-%d\') as paid_date2, DAY(paid_date) as dates '
        )
        // 'count(*) as total_transaksi',
        // 'sum(quantity) as total_sales',
        // 'DATE_FORMAT(paid_date,\'%Y-%m-%d\') as paid_date2',
        // 'DAY(paid_date) as dates',
    )
        .from('ecommerce_transaction')
        .where('transaction_status', 'settlement')
        .andWhereBetween('paid_date', [dateLast7Days, dateNow])
        .groupBy('paid_date2', 'dates')
        .orderBy('paid_date2', 'asc')
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil data user",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }
        });
    // res.json({
    //     message: 'Post Created',
    //     // authData
    // })

});

router.get('/weekly-sales', (req, res) => {
    // let page = req.params.page;
    let dateNow = moment().format('YYYY-MM-DD')
    let dateLast7Days = moment().subtract('7', 'days').format('YYYY-MM-DD')
    // console.log(moment().subtract('7', 'days').format('YYYY-MM-DD'))
    // console.log(moment().format('YYYY-MM-DD'))
    db.select(
        '*',
    )
        // .count('*')
        .from('ecommerce_transaction')
        .where('transaction_status', 'settlement')
        // .andWhereBetween('paid_date', [dateNow, dateLast7Days])
        // .groupBy('paid_date2')
        // .orderBy('paid_date2', 'desc')
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil data user",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }
        });
    // res.json({
    //     message: 'Post Created',
    //     // authData
    // })

});


router.get('/mycashback', (req, res) => {
    // let page = req.params.page;
    let id_user = req.userData.id
    // let remember_token = req.body.remember_token
    db.select(
    'point_cash_back'
    )
        .from('members')
        .where('id', id_user)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil cashback user",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }
        });
    // res.json({
    //     message: 'Post Created',
    // })

});

module.exports = router;