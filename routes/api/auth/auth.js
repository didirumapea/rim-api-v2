const express = require('express');
const router = express.Router();
const db = require('../../../database').db_rim; // as const knex = require('knex')(config);
const moment = require('moment');
const config = require('../../../config/config')
const setupPaginator = require('knex-paginator');
const checkAuth = require('../../../middleware/check-auth')
const jwt = require('jsonwebtoken')
const randomstring = require('randomstring')
const generateBarcode = require('../../../config/global-function').genBarcode
const mailing = require('../../../plugins/mailing')

// bcrypt config
const bcrypt = require('bcrypt');
const saltRounds = 10;
// const myPlaintextPassword = 's0/\/\P4$$w0rD';
// const someOtherPlaintextPassword = 'not_bacon';
setupPaginator(db);


let dateNow = moment().format("YYYY-MM-DD hh:mm:ss")

router.get('/', (req, res) => {
    res.send('We Are In Auth Route')
})

//
router.post('/user/login',  (req, res) => {
    // let page = req.params.page;
    let email = req.body.email
    let password = req.body.password
    // console.log(req.body)

    db.select(
        'id',
        'fullname',
        'email',
        'password',
        'phone',
        'birthday',
        'birthplace',
        'living',
        'photo',
        'ref_code',
        'gender',
        'point_cash_back',
        'ref_code'
        )
        .from('members')
        .where('email', email)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            // console.log(data)
            if (data.length > 0){
                bcrypt.compare(password, data[0].password).then((result) => {
                    if (result){
                        const user = {
                            id: data[0].id,
                            name: data[0].fullname,
                            email: data[0].email,
                            phone: data[0].phone,
                            birthday: data[0].birthday,
                            birthplace: data[0].birthplace,
                            living: data[0].living,
                            photo: data[0].photo,
                            ref_code: data[0].ref_code,
                            gender: data[0].gender,
                            point_cash_back: data[0].point_cash_back,
                        }
                        jwt.sign(user, config.jwtSecretKey, {expiresIn: '30d'}, (err, token) => {
                            // console.log(token)
                            // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                            user.token = token
                            // console.log(user)
                            // req.session.authUser = { user }
                            res.json({
                                success: true,
                                message: "Login sucessful",
                                // count: data.length,
                                data: user
                            });
                        });
                    }else{
                        res.json({
                            success: result,
                            message: "Password salah.",
                            // data: data,
                        })
                    }
                    // console.log(res)
                });


            }else {
                res.json({
                    success: false,
                    message: "Email salah atau belum terdaftar",
                    // data: data,
                })
            }
        })

});
// CHECK EMAIL
router.get('/user/check-email/email=:email',  (req, res) => {
    // let page = req.params.page;
    let email = req.params.email
    // let password = req.body.password
    // console.log(req.params)

    db.select(
        'id',
        'fullname',
        'email',
        'password'
    )
        .from('members')
        .where('email', '=', email)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length > 0){
                res.json({
                    success: true,
                    message: "Email terdaftar",
                    // data: data,
                })
            }else{
                res.json({
                    success: false,
                    message: "Email salah atau belum terdaftar",
                    // data: data,
                })
            }
            // console.log(data)
        })

});
// REGISTER
router.post('/user/register', (req, res) => {
    // let account_type = req.body.account_type
    let _data = {
        email: req.body.email,
        phone: req.body.phone,
        fullname: req.body.fullname,
        password: req.body.password,
        birthplace: req.body.birth_place,
        living: req.body.living,
        photo: req.body.photo,
        gender: req.body.gender,
        register_code: req.body.register_code,
    }

    // console.log(_data)
    if (_data.register_code !== '' || _data.register_code.length !== 0 ){
            db.select(
            '*'
            )
            .from('mst_code')
            .where('code_name', _data.register_code)
            .andWhere('activated', 1)
            .then(data => {
                if (data.length !== 0) {
                    if (data[0].used < data[0].quantity) {
                        console.log('Auth.js: Register code bisa digunakan')
                        registerUser(_data, res, (response) => {
                            db('mst_code')
                                .where('id', data[0].id)
                                .andWhere('code_name', data[0].code_name)
                                .update({used: data[0].used + 1})
                                .then(data2 => {
                                    db('members')
                                        .where('id', response.id)
                                        .andWhere('email', response.email)
                                        .update({point_cash_back: data[0].code_price})
                                        .then(data3 => {
                                            console.log('Update point cash back success '+data3)
                                            // console.log(response)
                                        })
                                    console.log('Update used success '+data2)
                                    // console.log(response)
                                })
                        })

                    } else {
                        // kode sudah mencapai batas limit
                        res.json({
                            success: false,
                            message: "Code sudah mencapai batas limit",
                            // data: data,
                        })
                    }
                } else {
                    res.json({
                        success: false,
                        message: "Code tidak di temukan",
                        // data: data,
                    })
                }
            })
            .catch((err) => {
                console.log(err)
            })
    }else{
        registerUser(_data, res)
    }

});

router.get('/user/detail', checkAuth, (req, res) => {
    // let page = req.params.page;
    // let id = req.body.id
    // let remember_token = req.body.remember_token
    // console.log(req.userData)
    db.select(
        'id',
        'email',
        'phone',
        'fullname',
        'photo',
        'living',
        'gender',
        'point_cash_back',
        'ref_code',
        'birthplace',
        'birthday',
        )
        .from('members')
        // .leftJoin('t_order as t2', 't1.id', 't2.id_user')
        .where('id', '=', req.userData.id)
        // .andWhere('remember_token', '=', remember_token)
        // .orderBy('end_date', 'desc')
        // .limit(1)
        // .paginate(limit, page, true)
        .then(data => {
            data[0].token = req.headers.authorization.split(' ')[1]
            if (data.length === 0){
                res.json({
                    // user: req.userData
                    success: false,
                    message: "Data tidak di temukan",
                    // count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil data user",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }
        });
//
});

router.post('/user/edit', checkAuth, (req, res) => {
    console.log(req.body)
    let details = {
        email: req.body.email,
        phone: req.body.phone,
        // fullname: req.body.fullname,
        // birthday: req.body.birthday,
        // birthplace: req.body.birthplace,
        living: req.body.living,
        gender: req.body.gender,
        photo: req.body.photo,

    }

    db('members')
        .where('id', req.userData.id)
        .update(details)
        .then(data => {
            res.json({
                success: true,
                message: "Update user berhasil.",
                count: data.length,
                data: data,
            });

        }).catch((err) =>{
        console.log(err)
        res.json({
            success: false,
            message: "Update user failed.",
            // count: data.length,
            data: err,
        });
    });
//
});

// EDITED NAME ONLY
router.post('/user/edit/name=:name', checkAuth, (req, res) => {
    let details = {
        fullname: req.params.name,
    }

    db('members')
        .where('id', req.userData.id)
        .update(details)
        .then(data => {
            res.json({
                success: true,
                message: "Update user name berhasil.",
                count: data.length,
                data: data,
            });

        }).catch((err) =>{
        console.log(err)
        res.json({
            success: false,
            message: "Update user failed.",
            // count: data.length,
            data: err,
        });
    });
//
});
// RESET PASSWORD
router.post('/user/reset-password', (req, res) => {
    let email = req.body.email
    db.select(
        'id',
        'fullname',
        'email',
    )
        .from('members')
        .where('email', email)
        .then(data => {
            if (data.length === 0){
                res.json({
                    // user: req.userData
                    success: false,
                    message: "Email tidak di temukan",
                });
            }else{
                let newPassword = randomstring.generate({
                    length: 8,
                    capitalization: 'uppercase',
                    charset: 'hex'
                });
                console.log('Auth.js: '+newPassword)
                bcrypt.genSalt(saltRounds, (err, salt) => {
                    bcrypt.hash(newPassword, salt, (err, hash) => {
                        db('members')
                            .where('email', email)
                            .update({password: hash})
                            .then(data2 => {
                                mailing.sendEmailResetPassword(data[0].email, newPassword, 'Request reset password rimember', 'reset-password-member', data[0].fullname)
                                res.json({
                                    success: true,
                                    message: "Success Change Password User",
                                    // isMatch: isMatch,
                                    password: hash,
                                    data: data2
                                })

                            }).catch((err) =>{
                                console.log(err)
                                res.json({
                                    success: false,
                                    message: "Reset password failed.",
                                    // count: data.length,
                                    data: err,
                                });
                        });
                    });
                });
            }
        });
//
});

// GANTI PASSWORD
router.post('/user/ganti-password',  (req, res) => {
    // let page = req.params.page;
    let email = req.body.email
    let oldPassword = req.body.old_password
    let newPassword = req.body.new_password
    // console.log(req.body)

    db.select(
        'id',
        'fullname',
        'email',
        'password',
        'phone',
        'birthday',
        'birthplace',
        'living',
        'photo',
        'ref_code',
        'gender',
        'point_cash_back',
        'ref_code'
    )
        .from('members')
        .where('email', email)
        .then(data => {
            // console.log(data)
            if (data.length > 0){
                bcrypt.compare(oldPassword, data[0].password).then((result) => {
                    if (result){
                        bcrypt.genSalt(saltRounds, (err, salt) => {
                            bcrypt.hash(newPassword, salt, (err, hash) => {
                                db('members')
                                    .where('email', email)
                                    .update({password: hash})
                                    .then(data2 => {
                                        // mailing.sendEmailResetPassword(data[0].email, newPassword, 'Request reset password rimember', 'reset-password-member', data[0].fullname)
                                        res.json({
                                            success: true,
                                            message: "Success Change Password User",
                                            // isMatch: isMatch,
                                            password: hash,
                                            data: data2
                                        })

                                    }).catch((err) =>{
                                    console.log(err)
                                    res.json({
                                        success: false,
                                        message: "Reset password failed.",
                                        // count: data.length,
                                        data: err,
                                    });
                                });
                            });
                        });
                    }else{
                        res.json({
                            success: result,
                            message: "Password salah.",
                            // data: data,
                        })
                    }
                    // console.log(res)
                });


            }else {
                res.json({
                    success: false,
                    message: "Email salah atau belum terdaftar",
                    // data: data,
                })
            }
        })

});


// CHECK REFERRAL CODE
router.get('/check/refferal-code=:ref_code',  (req, res) => {
    // let page = req.params.page;
    let ref_code = req.params.ref_code
    // let password = req.body.password
    // console.log(req.params)

    db.select(
        'id',
        'fullname',
        'email',
        'photo'
    )
        .from('members')
        .where('ref_code', ref_code)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length > 0){
                res.json({
                    success: true,
                    message: "Refcode Terdaftar",
                    data: data,
                })
            }else{
                res.json({
                    success: false,
                    message: "Refcode belum terdaftar",
                    // data: data,
                })
            }
            // console.log(data)
        }).
        then((err) => {
            console.log(err)
    })

});


// SAMPLE CODE
router.get('/sample/dowhile', (req, res) => {
    // test(res)
    checkRefCode((response) => {
        console.log(response)
    })
    async function checkRefCode2(){
        let i = 0;
        let rows;
        do {
            let random = randomstring.generate({
                length: 6,
                capitalization: 'uppercase',
                charset: 'hex'
            });
            rows = await db.select('*')
                .from('members')
                .where('ref_code', random);

            console.log(rows.length, i, random)
            i++;
            if (rows.length === 0){
                console.log('refcode belum terdaftar')
            }else{
                console.log('refcode sudah terdaftar')
            }

            // batasan   loop
            // if (i > 5){
            //     rows.length = 1;
            // }
        }
        while (rows.length !== 0);

        // console.log('its going end')
        // console.log(rows.length)
    }
    res.json({
        message: 'Post Created',
        data: 'looking good'
    })

})

async function checkRefCode(callback){
    let i = 0;
    let rows;
    do {
        let random = randomstring.generate({
            length: 6,
            capitalization: 'uppercase',
            charset: 'hex'
        });
        rows = await db.select('*')
            .from('members')
            .where('ref_code', random);

        // console.log(rows.length, i, random)
        i++;
        if (rows.length === 0){
            callback('RIM-'+random)
            console.log('refcode '+'RIM-'+random+' bisa di daftarkan')
        }else{
            console.log('refcode '+'RIM-'+random+' sudah terdaftar di database')
        }

        // batasan   loop
        // if (i > 5){
        //     rows.length = 1;
        // }
    }
    while (rows.length !== 0);
    // console.log('its going end')
    // console.log(rows.length)
}

registerUser = (_data, res, callback) => {
    db.select(
        'id',
        'fullname',
        'email',
    )
        .from('members')
        .where('email', '=', _data.email)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length === 0){
                bcrypt.genSalt(saltRounds, (err, salt) => {
                    bcrypt.hash(_data.password, salt, (err, hash) => {
                        // check REF Code IF Exist
                        checkRefCode((gen_ref_code) => {
                            // Store hash in your password DB.
                            db('members')
                                .insert({
                                    // account_type: account_type,
                                    email: _data.email,
                                    phone: _data.phone,
                                    fullname: _data.fullname,
                                    password: hash, // hash password
                                    // birthday: birthday,
                                    birthplace: _data.birth_place,
                                    living: _data.living,
                                    photo: _data.photo,
                                    gender: _data.gender,
                                    ref_code: gen_ref_code, // gen ref code
                                    register_code: _data.register_code
                                    // provider: provider
                                })
                                .then(data2 => {
                                    // console.log(data2[0])
                                    db.select(
                                        '*'
                                    )
                                        .from('members')
                                        .where('id', data2[0])
                                        .then((data3) => {
                                            // console.log(data3)
                                            // Mock User
                                            const user = {
                                                id: data3[0].id,
                                                name: data3[0].fullname,
                                                email: data3[0].email,
                                                phone: data3[0].image,
                                                birthday: data3[0].birthday,
                                                birthplace: data3[0].birthplace,
                                                living: data3[0].living,
                                                photo: data3[0].photo,
                                                ref_code: data3[0].ref_code,
                                                gender: data3[0].gender,
                                                point_cash_back: data3[0].point_cash_back,
                                            }
                                            jwt.sign(user, config.jwtSecretKey, {expiresIn: '30d'}, (err, token) => {
                                                // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                                                user.token = token
                                                generateBarcode.generateBarcode(gen_ref_code)
                                                res.json({
                                                    success: true,
                                                    message: "Daftar berhasil dan login sukses",
                                                    // count: data3.length,
                                                    // current_page: paginator.current_page,
                                                    // limit: paginator.data.length,
                                                    // sortBy: sortBy,
                                                    data: user
                                                    // token: token
                                                });
                                                return callback(user)
                                            });
                                        })
                                });
                        })
                    });
                });
            }else {
                res.json({
                    success: false,
                    message: "Email sudah terdaftar",
                    // data: data,
                })
            }
        })
        .catch((err) => {
            console.log(err)
        })
}


module.exports = router;