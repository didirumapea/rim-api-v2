const express = require('express');
const router = express.Router();
const db = require('../../../database').db_rim; // as const knex = require('knex')(config);
const moment = require('moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const date = require('../../../config/moment-date-format')
const splitImage = require('../../../config/global-function').splitImage
// const autocorrect = require('autocorrect')()


let dateNow = moment().format("YYYY-MM-DD hh:mm:ss")

router.get('/', (req, res) => {
    // console.log(req.query)
    res.send('We Are In BANK Route')
})

// REDEEM VOUCHER CODE
router.get('/list',  (req, res) => {
    let code = req.params.code
    let fid_promo = req.params.fid_promo
    // console.log(catId, page, limit, sort, min, max, rating)
    // console.log(req)
    db.select(
        '*'
    )
        .from('mst_bank_provider')
        .andWhere('activated', 1)
        .then(data => {
            res.json({
                // name: data
                success: true,
                message: 'Get list bank success',
                data: data
            });
        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'something error check your server logs.',
                data: err.toLocaleString()
            });
            console.log(err)
        });

});

module.exports = router;