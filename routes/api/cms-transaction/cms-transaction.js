const express = require('express');
const router = express.Router();
const db = require('../../../database').db_rim; // as const knex = require('knex')(config);
const moment = require('moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const date = require('../../../config/moment-date-format')
const payment = require('../../../plugins/payment')
// const pdfGen = require('../../../plugins/pdf-generator')


let dateNow = moment().format("YYYY-MM-DD hh:mm:ss")

router.get('/', (req, res) => {
    // console.log(req.query)
    res.send('We Are In CMS TRANSACTION Route')
})

// REDEEM VOUCHER CODE
router.get('/check/fid_promo=:fid_promo/code=:code',  (req, res) => {
    let code = req.params.code
    let fid_promo = req.params.fid_promo
    // console.log(catId, page, limit, sort, min, max, rating)
    // console.log(req)
    db.select(
        '*'
    )
        .from('mst_code')
        .where('code_name', code)
        .andWhere('activated', 1)
        .then(data => {
            // console.log(data)
            if (data.length !== 0){
                if (data[0].used < data[0].quantity){
                    switch (data[0].code_type) {
                        case 'category':
                            db.select(
                                't1.id'
                            )
                                .from('merchant_promo as t1')
                                .innerJoin('merchant as t2', 't1.fid_merchant', 't2.id')
                                .where('t1.id', fid_promo)
                                .andWhere('t2.fid_category', data[0].code_type_value)
                                .then((data2) => {
                                    if(data2.length > 0){
                                        res.json({
                                            success: true,
                                            message: 'Kode bisa digunakan pada category ini',
                                            data: data,
                                        });
                                    }else{
                                        res.json({
                                            success: false,
                                            message: 'Kode tidak bisa digunakan pada category ini',
                                            data: data2,
                                        });
                                    }
                                })
                            break;
                        case 'merchant':
                            db.select(
                                'id'
                            )
                                .from('merchant_promo')
                                .where('id', fid_promo)
                                .andWhere('fid_merchant', data[0].code_type_value)
                                .then((data2) => {
                                    if(data2.length > 0){
                                        res.json({
                                            success: true,
                                            message: 'Kode bisa digunakan pada merchant ini',
                                            data: data,
                                        });
                                    }else{
                                        res.json({
                                            success: false,
                                            message: 'Kode tidak bisa digunakan pada merchant ini',
                                            data: data2,
                                        });
                                    }
                                })
                            break;
                        case 'product':
                            // console.log(data[0].code_type_value.toString(), fid_promo)
                            if(data[0].code_type_value.toString() === fid_promo.toString()){
                                res.json({
                                    success: true,
                                    message: 'Kode bisa digunakan pada product ini',
                                    data: data,
                                });
                            }else{
                                res.json({
                                    success: false,
                                    message: 'Kode tidak bisa digunakan pada product ini',
                                    // data: data2,
                                });
                            }
                            break;
                        default:
                            res.json({
                                success: false,
                                message: 'ERROR, CEK UR CODE AT code.js',
                                // data: data2,
                            });
                            break;
                    }
                }else{
                    res.json({
                        success: false,
                        message: 'Code sudah mencapai limit',
                        // data: data,
                    });
                }

            }else{
                res.json({
                    // name: data
                    success: false,
                    message: "Data Kosong",
                    count: data.length,

                });
            }
        })
        .catch((err) => {
            res.json({
                // name: data
                success: false,
                message: 'something error check your server logs.',
                data: err.toLocaleString()
            });
            console.log(err)
        });

});

// LIST DIRECT TRANSACTION
router.get('/direct-transaction/page=:page/limit=:limit/sort=:sort', (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;

    db.select(
        't1.id',
        't1.fid_user',
        't1.fid_promo',
        't1.inv_code',
        't1.created as order_date',
        't1.paid_date',
        't1.total',
        't1.transaction_status',
        't1.bank_name',
        't1.bank_account_name',
        't1.bank_account_no',
        't1.quantity',
        't1.cashback',
        't2.fullname',
        't2.email',
        't2.email',
        't3.title as merchant_name',
    )
        .from('ecommerce_transaction as t1')
        .innerJoin('members as t2', 't1.fid_user', 't2.id')
        .innerJoin('merchant as t3', 't1.fid_merchant', 't3.id')
        .where('t1.activated', 1)
        .andWhere('t1.payment_type', 'direct_transfer')
        .orderBy('t1.id', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            // paginator.data[0].coordinat2 = JSON.parse(paginator.data[0].coordinat2)
            // console.log(paginator)
            res.json({
                success: true,
                message: "sukses ambil data direct transfer by id merchant",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: sortBy,
                data: paginator.data,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                err: err
            })
        })


});

// SETTLE VOUCHER DIRECT
router.post('/settle-voucher-direct',(req, res) => {
    console.log(req.body.fid_user, req.body.inv_code, req.body.fid_promo )
    let _data= {
        transaction_status: 'settlement',
        paid_date: date.utc()
    }
    db('ecommerce_transaction')
        .where('fid_promo', req.body.fid_promo)
        .andWhere('fid_user', req.body.fid_user)
        .andWhere('inv_code', req.body.inv_code)
        .andWhere('transaction_status', 'unverification')
        .update(_data)
        .then(data => {
            if(data > 0){
                payment.transSettle(req.body.inv_code)
                res.json({
                    success: true,
                    message: "Settle direct trans success",
                    data: data,
                    // redirect_url: redirectUrl
                })
            }else{
                res.json({
                    success: false,
                    message: "Settle direct trans not found",
                    data: data,
                    // redirect_url: redirectUrl
                })
            }

            // console.log('Settle : Update cashback success')
        }).catch((err) => {
        console.log('Settle : ' + err)
    });





});

module.exports = router;