const express = require('express');
const app = express();
const apiRoute = require('./routes/api');
const version = 'v2/'
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const admin = require("firebase-admin");
const serviceAccount = require("./public/rim-web-v2-1571640333794-firebase-adminsdk-uogzg-6a873d99ab");

// const session = require('express-session')
// const sshtunnel = require('./database/ssh-tunnel-db')

// use it before all route definitions
app.use(cors());
app.use(express.static('/mnt/cdn'));
// Sessions untuk membuat `req.session`
// app.use(session({
//     secret: 'super-secret-key',
//     resave: false,
//     saveUninitialized: false,
//     cookie: { maxAge: 60000 }
// }))



admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://rim-web-v2-1571640333794.firebaseio.com"
});

app.get('/', function (req, res) {
    res.send('Hello Rimember V2 WEB API')
})

app.get('/api/'+version, function (req, res) {
    res.send('Hello Rimember V2 WEB API With Version')
})


// catch 404 and forward to error handler global
// app.use(function(req, res, next) {
//     return res.status(404).json({
//         success : 'false',
//         message :'Request tidak di Temukan'
//     });
//     let err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });




app.use(bodyParser.urlencoded({
    extended: true,
    // limit: '50mb'
}));
app.use(bodyParser.json({
    // limit: '50mb'
}));


app.use('/api/'+version, apiRoute); //ex localhost:3002/api/merchant/get/list/1


app.use(cookieParser());
// app.listen('3002')
const PORT = process.env.PORT || process.env.PORT
app.listen(PORT, console.log(`Server started on port ${PORT}`))